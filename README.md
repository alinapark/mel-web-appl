Zhastar Play
===================


Web application. Main purpose of the project is to prevent suicides among children. Application allows psychologists, psychiatrists, teachers and general doctors to identify suicidal tendencies of the student on early stages.

----------
Technologies
-------------

- Play Framework 1.4.4
- NodeJs v8.4.0
- Apache Maven 3.5.0
- MySQL 5.7.19

----------
Branches
-------------

- **dev** - development
- **master** - production

----------
Project structure
-------------
 

------ app //Source code  
------------ controllers   //Request processing and main logic  
------------ dto               //DTO classes  
------------ enums         //Enumerations  
------------ models        //Classes representing DB tables  
------------ utils            //Utility and helper classes  
------------ views          //Views, html files  
------ conf //Configurations  
------------ application.conf      //Config file  
------------ dependencies.yml   //Maven dependencies  
------------ log4j.properties      //Logger configurations  
------------ messages.en           //English translations  
------------ messages.kz           //Kazakh translations  
------------ messages.ru           //Russian translations  
------------ routes   //Routes  
------ lib //Libraries  
------ logs //Logs  
------ public //Public assets: js, css, images  
------ resources //  
------ sql //Sql scripts  
    
----------
Configuring IDE
------------
Play provides a commands for transforming Play application to different IDE projects.

**Eclipse **
> $ play eclipsify

**NetBeans**
> $ play netbeansify

**IntelliJ IDEA**
> $ play idealize

More information on official site, [Setting-up your preferred IDE](https://www.playframework.com/documentation/1.4.x/ide)

----------
Installation
------------

To install all Maven dependencies run the following command
> $ play deps --clearcache

To install all NodeJs dependencies run the following command
> $ npm install

To compile all SASS files

> $ npm run compile

To run project 
> $ play run

To run project in background
> $ play start

To start project in **prod** mode
> $ play start --%prod

To start project in **test** mode
> $ play start --%test

After starting application is available at address http://localhost:9000/

----------
Configurations
------------