package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import dto.OfflineStudentsArrayDto;
import enums.Gender;
import enums.Language;
import models.*;

import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import play.i18n.Lang;
import play.mvc.Before;
import play.mvc.Controller;

import utils.BCrypt;
import utils.ClientControllerHelper;
import utils.SurveyUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tauyekel on 5/31/17.
 */
public class Admin extends Controller {

    private static final String fileDirectory = Play.configuration.getProperty("fileDirectory");
    private static final String documentsPath = "/storage/documents/";
    private static final String lessonsPath = "/storage/lessons/";
    private static final String newsPath = "/storage/news/";
    private static final Integer pageLength = 30;

    private static class GroupWithGeneratedAccounts {
        String name;
        public List<Account> accounts = new ArrayList<Account>();
        public GroupWithGeneratedAccounts(String name) {
            this.name = name;
        }
    }


    @Before
    private static void checkSecurity() {
        if (session.get("adminLogged") == null) {
            Application.adminLogin();
        }
        renderArgs.put("admin", session.get("adminLogged"));
        if (play.i18n.Lang.get().equals("kz")) {
            renderArgs.put("lang", "kz");
        } else {
            renderArgs.put("lang", "ru");
        }
        List<Group> groups = Group.findAll();
        List<Location> regions = (ArrayList<Location>) Cache.get("regions");
        if (regions == null) {
            regions = new ArrayList<Location>();
            List<Location> locations = Location.findAll();
            for (Location location : locations) {
                if (location.parent0 != null && location.parent1 == null && location.parent2 == null) {
                    regions.add(location);
                }
            }
            Cache.add("regions", regions, "8h");
        }
        renderArgs.put("regions", regions);
        renderArgs.put("modalGroups", groups);
    }

    public static void changeLang(String lang) {
        Lang.change(lang);
        accounts(1, null, null, null, null, null, null);
    }

    public static void accountInfo(Integer id) {
        Account account = Account.findById(id);
        Profile profile = Profile.find("account = ?1", account).first();
        if (account.isLastQuizFinished) {
            renderArgs.put("lastFinished", true);
            QuizScore quizScoreLast = QuizScore.find("Select q from QuizScore q where q.isPassed = true and q.step.position = 1000 and q.account = :user").setParameter("user", account).first();
            QuizScore quizScoreFirst = QuizScore.find("Select q from QuizScore q where q.isFinished = true and q.step.position = 0 and q.account = :user").setParameter("user", account).first();
            Integer percFirst = Math.round((float) quizScoreFirst.totalRight / (quizScoreFirst.totalWrong + quizScoreFirst.totalRight) * 100);
            Integer percLast = Math.round((float) quizScoreLast.totalRight / (quizScoreLast.totalWrong + quizScoreLast.totalRight) * 100);
            renderArgs.put("percFirst", percFirst);
            renderArgs.put("percLast", percLast);
        }
        renderTemplate("/admin/accounts/info.html", account, profile);
    }

    public static void results(Integer page,
                               Integer idFilter,
                               String loginFilter,
                               Integer groupsFilterId,
                               Integer regionDropdown,
                               Integer provinceDropdown,
                               Integer cityDropdown) {
        String action = "/admin/results";
        String idInputName = "idFilter";
        String loginInputName = "loginFilter";
        String groupsDropdownName = "groupsFilterId";
        String regionDropdownName = "regionDropdown";
        String provinceDropdownName = "provinceDropdown";
        String cityDropdownName = "cityDropdown";
        List<Group> groupsFilter = Group.findAll();
        String active = "results";

        renderArgs.put("results", true);
        List<Account> accounts = null;
        if (idFilter != null) {
            renderArgs.put("idFilter", idFilter);
            //find by id if provided
            Account account = Account.findById(idFilter);
            if (account != null) {
                accounts = new ArrayList<Account>();
                accounts.add(account);
            }
        } else if (loginFilter != null && !loginFilter.trim().equals("")) {
            renderArgs.put("loginFilter", loginFilter);
            accounts = Account.find("UPPER(username) like ?1", "%" + loginFilter.toUpperCase() + "%").fetch();
        } else {
            Group group = null;
            Location city = null;
            Location province = null;
            Location region = null;
            if (groupsFilterId != null) {
                group = Group.findById(groupsFilterId);
            }
            if (cityDropdown != null) {
                city = Location.findById(cityDropdown);
            }
            if (provinceDropdown != null) {
                province = Location.findById(provinceDropdown);
            }
            if (regionDropdown != null) {
                region = Location.findById(regionDropdown);
            }
            CriteriaBuilder criteriaBuilder = JPA.em().getCriteriaBuilder();
            CriteriaQuery<Account> query = criteriaBuilder.createQuery(Account.class);
            Root<Account> root = query.from(Account.class);
            query.select(root);
            List<Predicate> predicates = new ArrayList<Predicate>();
            if (group != null) {
                renderArgs.put("groupName", group);
                predicates.add(criteriaBuilder.equal(root.get("group"), group));
            }
            if (city != null) {
                renderArgs.put("cityName", city);
                predicates.add(criteriaBuilder.equal(root.get("school").get("city"), city));
            }
            if (region != null) {
                renderArgs.put("regionName", region);
                predicates.add(criteriaBuilder.equal(root.get("school").get("region"), region));
            }
            if (province != null) {
                renderArgs.put("provinceName", province);
                predicates.add(criteriaBuilder.equal(root.get("school").get("province"), province));
            }
            if (predicates.size() != 0) {
                Predicate[] array = new Predicate[predicates.size()];
                query.where(predicates.toArray(array));
                accounts = JPA.em().createQuery(query).getResultList();
            }
        }
        if (accounts == null) {
            accounts = new ArrayList<Account>();
        }
        renderTemplate("admin/results/index.html",
                accounts,
                action,
                groupsFilter,
                active,
                idInputName,
                loginInputName,
                groupsDropdownName,
                regionDropdownName,
                provinceDropdownName,
                cityDropdownName);
    }

    public static void accounts(Integer page,
                                Integer idFilter,
                                String loginFilter,
                                Integer groupsFilterId,
                                Integer regionDropdown,
                                Integer provinceDropdown,
                                Integer cityDropdown) {
        if (page == null || page < 1) {
            accounts(1, null, null, null, null, null, null);
        }
        Long pageNumber = 1L;
        List<Account> accounts = null;
        if (idFilter != null) {
            renderArgs.put("idFilter", idFilter);
            //find by id if provided
            Account account = Account.findById(idFilter);
            if (account != null) {
                accounts = new ArrayList<Account>();
                accounts.add(account);
            }
        } else if (loginFilter != null && !loginFilter.trim().equals("")) {
            //if username provided, find username by login
            renderArgs.put("loginFilter", loginFilter);
            pageNumber = (Account.count("UPPER(username) like ?1", "%" + loginFilter.toUpperCase() + "%") + pageLength - 1) / pageLength;
            accounts = Account.find("UPPER(username) like ?1", "%" + loginFilter.toUpperCase() + "%").fetch(page, pageLength);
        } else {
            Location region;
            Location province;
            Location city;
            Group group = null ;
            if (groupsFilterId != null) {
                group = Group.findById(groupsFilterId);
                renderArgs.put("groupName", groupsFilterId);
            }
            if (cityDropdown == null && regionDropdown == null && provinceDropdown == null) {
                if (group == null) {
                    pageNumber = (Account.count() + pageLength - 1) / pageLength;
                    accounts = Account.find("").fetch(page, pageLength);
                } else {
                    pageNumber = (Account.count("group = ?1", group) + pageLength - 1) / pageLength;
                    accounts = Account.find("group = ?1", group).fetch(page, pageLength);
                }
            } else if (cityDropdown != null) {
                renderArgs.put("cityDropdown", cityDropdown);
                city = Location.findById(cityDropdown);
                if (group == null) {
                    pageNumber = (Account.count("school.city = ?1", city) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.city = ?1", city).fetch(page, pageLength);
                } else {
                    pageNumber = (Account.count("school.city = ?1 and group = ?2", city, group) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.city = ?1 and group = ?2", city, group).fetch(page, pageLength);
                }
            } else if (provinceDropdown != null) {
                renderArgs.put("provinceDropdown", provinceDropdown);
                province = Location.findById(provinceDropdown);
                if (group == null) {
                    pageNumber = (Account.count("school.province = ?1", province) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.province = ?1", province).fetch(page, pageLength);
                } else {
                    pageNumber = (Account.count("school.province = ?1 and group = ?2", province, group) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.province = ?1 and group = ?2", province, group).fetch(page, pageLength);
                }
            } else if (regionDropdown != null) {
                renderArgs.put("regionDropdown", regionDropdown);
                region = Location.findById(regionDropdown);
                if (group == null) {
                    pageNumber = (Account.count("school.region = ?1", region) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.region = ?1", region).fetch(page, pageLength);
                } else {
                    pageNumber = (Account.count("school.region = ?1 and group = ?2", region, group) + pageLength - 1) / pageLength;
                    accounts = Account.find("school.region = ?1 and group = ?2", region, group).fetch(page, pageLength);
                }
            }
            //find group by provided id and find all accounts related to that group
        }
        //input arguments
        String action = "/admin/accounts";
        String idInputName = "idFilter";
        String loginInputName = "loginFilter";
        String groupsDropdownName = "groupsFilterId";
        String regionDropdownName = "regionDropdown";
        String provinceDropdownName = "provinceDropdown";
        String cityDropdownName = "cityDropdown";
        List<Group> groupsFilter = Group.findAll();
        String active = "accounts";

        if (accounts == null) {
            accounts = new ArrayList<Account>();
        }
        renderTemplate("admin/accounts/index.html", action, page, pageNumber, accounts, active, groupsFilter, idInputName, loginInputName, groupsDropdownName, regionDropdownName, provinceDropdownName, cityDropdownName);
    }

    public static void accountsCreate(String duplicateError, String emptyFieldsError) {
        List<Group> groups = Group.findAll();
        String actionButton = "Добавить запись";
        String active = "accounts";

        //input names for templated inputs
        String loginInputName = "login";
        String passwordInputName = "password";
        String groupInputName = "groupId";
        String schoolInputName = "schoolId";
        String regionDropdownName = "regionDropdown";
        String provinceDropdownName = "provinceDropdown";
        String cityDropdownName = "cityDropdown";

        renderTemplate("admin/accounts/create.html", groups, actionButton, active, loginInputName, passwordInputName, groupInputName, schoolInputName, duplicateError, emptyFieldsError, regionDropdownName, provinceDropdownName, cityDropdownName);
    }

    public static void accountsShow(Integer generatedBatch) {
        List<Account> accounts = Account.find("generatedBatch = ?1", generatedBatch).fetch();
        School school = accounts.get(0).school;
        List<GroupWithGeneratedAccounts> groups = new ArrayList<>();
        boolean inList;
        for (Account account: accounts) {
            inList = false;
            for (GroupWithGeneratedAccounts group : groups) {
                if (group.name.equals(account.group.name)) {
                    group.accounts.add(account);
                    inList = true;
                    break;
                }
            }
            if (!inList) {
                GroupWithGeneratedAccounts group = new GroupWithGeneratedAccounts(account.group.name);
                group.accounts.add(account);
                groups.add(group);
            }
        }
        renderTemplate("admin/accounts/show.html", accounts, school, groups);
    }

    public static void newAccount(String login, String password, String groupId, String schoolId) {
        Integer groupIdInt = null;
        Integer schoolIdInt = null;
        try {
            groupIdInt = Integer.parseInt(groupId);
            schoolIdInt = Integer.parseInt(schoolId);
        } catch (Exception e) {
            accountsCreate(null, "Не все поля заполнены или заполнены некорреткно");
        }
        Group group = Group.findById(groupIdInt);
        School school = School.findById(schoolIdInt);

        if (login != null && password != null && group != null && school != null) {
            String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

            Account duplicate = Account.find("username = ?1", login).first();
            if (duplicate != null) {
                String duplicateError = "Данный логин занят!";
                accountsCreate(duplicateError, null);
            }

            Account newAccount = new Account(login, hashedPassword, group, school, null);
            newAccount.save();
            accounts(1, null, null, null, null, null, null);
        } else {
            accountsCreate(null, "Не все поля заполнены или заполнены некорреткно");
        }
    }

    public static void accountsEdit(Integer accountId, String duplicateError, String emptyFieldsError) {
        Account account = Account.findById(accountId);
        List<Group> groups = Group.findAll();

        List <Location> provinces = Location.find("parent0 = ?1", account.school.region).fetch();
        List <Location> cities = Location.find("parent0 = ?1", account.school.province).fetch();
        List<School> schools = School.find("city = ?1", account.school.city).fetch();
        if (schools.size() == 0) {
            schools = School.find("province = ?1", account.school.province).fetch();
        }
        if (schools.size() == 0) {
            schools = School.find("region = ?1", account.school.region).fetch();
        }
        String loginInputName = "login";
        String passwordInputName = "password";
        String groupInputName = "groupId";
        String schoolInputName = "schoolId";

        String active = "accounts";
        renderTemplate("admin/accounts/edit.html", account, groups, schools, provinces, cities, loginInputName, passwordInputName, groupInputName, schoolInputName, active, duplicateError, emptyFieldsError);
    }

    public static void updateAccount(Integer accountId, @Required String login, String password, @Required Integer groupId, @Required Integer schoolId) {
        if (Validation.hasErrors()) {
            accountsEdit(accountId, null, "Не все поля заполнены или заполнены некорреткно");
        }

        Group group = Group.findById(groupId);
        School school = School.findById(schoolId);

        Account account = Account.findById(accountId);
        Account duplicate = Account.find("username = ?1", login).first();

        if (duplicate != null && duplicate.id != accountId) {
            accountsEdit(accountId, "Данный логин занят!", null);
        }
        account.username = login;
        account.group = group;
        account.school = school;
        if (password != null && !password.equals("")) {
            account.password = BCrypt.hashpw(password, BCrypt.gensalt());;
        }
        account.save();
        accounts(1, null, null, null, null, null, null);
    }

    public static void removeAccount(Integer accountId) {
        Account account = Account.findById(accountId);
        account.delete();
        accounts(1, null, null, null, null, null, null);
    }

    public static void groups(String idFilter, String nameFilter, String keyFilter) {
        List<Group> groups = new ArrayList<Group>();
        String active = "groups";

        //input names
        String idInputName = "idFilter";
        String nameInputName = "nameFilter";
        String keyInputName = "keyFilter";

        //handle filtering
        if (idFilter != null && !idFilter.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(idFilter);
            } catch (Exception e) {

            }
            if (id != null) {
                Group group = Group.findById(id);
                if (group != null) {
                    groups.add(group);
                }
            }
        } else if (nameFilter != null && !nameFilter.equals("")) {
            Group group = Group.find("name = ?1", nameFilter).first();
            if (group != null) {
                groups.add(group);
            }
        } else if (keyFilter != null && !keyFilter.equals("")) {
            Group group = Group.find("key = ?1", keyFilter).first();
            if (group != null) {
                groups.add(group);
            }
        } else {
            groups = Group.findAll();
        }
        renderTemplate("admin/groups/index.html", groups, active, idInputName, nameInputName, keyInputName);
    }

    public static void groupsCreate(String duplicateNameError, String duplicateKeyError, String emptyFieldsError) {

        //input names
        String nameInputName = "nameCreate";
        String keyInputName = "keyCreate";
        String action = "Добавить запись";

        String active = "groups";

        renderTemplate("admin/groups/create.html", nameInputName, keyInputName, active, action, duplicateKeyError, duplicateNameError, emptyFieldsError);
    }

    public static void newGroup(String nameCreate, String keyCreate) {
        if (nameCreate != null && !nameCreate.equals("") && keyCreate != null && !keyCreate.equals("")) {
            //check if name or group is taken
            Group checkNameGroup = Group.find("name = ?1", nameCreate).first();
            Group checkKeyGroup = Group.find("key = ?1", keyCreate).first();

            //re-render page with error messages if any
            if (checkNameGroup != null && checkKeyGroup == null) {
                groupsCreate("Это имя уже занято", null, null);
            } else if (checkNameGroup == null && checkKeyGroup != null) {
                groupsCreate(null, "Этот ключ уже занят", null);
            } else if (checkNameGroup != null && checkKeyGroup != null) {
                groupsCreate("Это имя уже занято", "Этот ключ уже занят", null);
            }
            Group group = new Group(nameCreate, keyCreate);
            group.save();
            accounts(1, null, null, null, null, null, null);
        } else {
            groupsCreate(null, null, "Не все поля заполнены или заполнены некорректно");
        }
    }

    public static void groupsEdit(Integer groupId, String duplicateNameError, String duplicateKeyError, String emptyFieldsError) {
        Group group = Group.findById(groupId);
        //input names
        String nameInputName = "nameCreate";
        String keyInputName = "keyCreate";
        String action = "Сохранить изменения";

        String active = "groups";

        renderTemplate("admin/groups/edit.html", group, nameInputName, keyInputName, action, active, duplicateNameError, duplicateKeyError, emptyFieldsError);
    }

    public static void updateGroup(Integer groupId, @Required String nameCreate, @Required String keyCreate) {
        if (Validation.hasErrors()) {
            groupsEdit(groupId, null, null, "Не все поля заполнены или заполнены некорректно");
        }
        Group group = Group.findById(groupId);
        Group duplicateNameGroup = Group.find("name = ?1", nameCreate).first();
        if (duplicateNameGroup != null) {
            groupsEdit(groupId, "Это имя уже занято", null, null);
        }
        Group duplicateKeyGroup = Group.find("key = ?1", keyCreate).first();
        if (duplicateKeyGroup != null) {
            groupsEdit(groupId, null, "Этот ключ уже занят", null);
        }
        group.name = nameCreate;
        group.key = keyCreate;
        group.save();
        groups(null, null, null);
    }

    public static void removeGroup(Integer groupId) {
        Group group = Group.findById(groupId);
        if (group.accounts.isEmpty()) {
            group.delete();
        } else {
            groups(null, null, null);
        }
    }

    public static void users(String idFilter, String emailFilter) {
        List<AdminUser> admins = new ArrayList();

        //input names
        String idInputName = "idFilter";
        String emailInputName = "emailFilter";

        //handle filtering
        if (idFilter != null && !idFilter.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(idFilter);
            } catch (Exception e) {
            }
            if (id != null) {
                AdminUser admin = AdminUser.findById(id);
                if (admin != null) {
                    admins.add(admin);
                }
            }
        } else if (emailFilter != null && !emailFilter.equals("")) {
            AdminUser admin = AdminUser.find("email = ?1", emailFilter).first();
            if (admin != null) {
                admins.add(admin);
            }
        } else {
            admins = AdminUser.findAll();
        }
        String active = "admins";
        renderTemplate("admin/users/index.html", active, admins, idInputName, emailInputName);
    }

    public static void usersCreate(String duplicateEmailError, String emptyFieldsError) {

        //input names and other
        String active = "users";
        String action = "Добавить администратора";
        String emailInputName = "emailCreate";
        String passwordInputName = "passwordCreate";

        renderTemplate("admin/users/create.html", active, action, emailInputName, passwordInputName, duplicateEmailError, emptyFieldsError);
    }

    public static void newUser(String emailCreate, String passwordCreate) {
        if (emailCreate != null && !emailCreate.equals("") && passwordCreate != null && !passwordCreate.equals("")) {
            //check if name or group is taken
            AdminUser checkEmailAdmin = AdminUser.find("email = ?1", emailCreate).first();
            //re-render page with error messages if any
            if (checkEmailAdmin != null) {
                usersCreate("Этот email уже занят", null);
            }
            //hash password
            String passwordHash = BCrypt.hashpw(passwordCreate, BCrypt.gensalt());
            AdminUser admin = new AdminUser(emailCreate, passwordHash);
            admin.save();
            accounts(1, null, null, null, null, null, null);
        } else {
            usersCreate(null, "Не все поля заполнены или заполнены некорректно");
        }
    }

    public static void usersEdit(Integer userId, String duplicateEmailError, String emptyFieldsError) {
        AdminUser admin = AdminUser.findById(userId);

        //input names
        String active = "users";
        String action = "Сохранить изменения";
        String emailInputName = "emailCreate";
        String passwordInputName = "passwordCreate";

        renderTemplate("admin/users/edit.html", admin, duplicateEmailError, emptyFieldsError, active, action, emailInputName, passwordInputName);
    }

    public static void updateUser(Integer userId, @Required String emailCreate, String passwordCreate) {
        if (Validation.hasErrors()) {
            usersEdit(userId, null, "Не все поля заполнены или заполнены некорректно");
        }
        AdminUser admin = AdminUser.findById(userId);
        AdminUser duplicateEmailAdmin = AdminUser.find("email = ?1", emailCreate).first();
        if (duplicateEmailAdmin != null) {
            usersEdit(userId, "Этот имейл уже занят", null);
        }
        admin.email = emailCreate;
        if (passwordCreate != null && !passwordCreate.equals("")) {
            admin.password = BCrypt.hashpw(passwordCreate, BCrypt.gensalt());
        }
        admin.save();
        users(null, null);
    }

    public static void removeUser(Integer userId) {
        AdminUser admin = AdminUser.findById(userId);
        admin.delete();
        users(null, null);
    }

    public static void steps(String idInput, String nameInput, String groupDropdown) {

        List<Step> steps = new ArrayList<Step>();
        String active = "steps";

        //input names and others
        String idInputName = "idInput";
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();

        //handle filtering
        if (idInput != null && !idInput.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(idInput);
            } catch (Exception e) {

            }
            if (id != null) {
                Step step = Step.findById(id);
                if (step != null && step.deletedAt == null) {
                    steps.add(step);
                }
            }
        } else if (nameInput != null && !nameInput.equals("")) {
            Step step = Step.find("name = ?1 and deletedAt = null", nameInput).first();
            if (step != null) {
                steps.add(step);
            }
        } else if (groupDropdown != null && !groupDropdown.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(groupDropdown);
            } catch (Exception e) {

            }
            Group group = Group.findById(id);
            if (group != null) {
                List<Step> stepsByGroup = Step.find("group = ?1 and deletedAt = null", group).fetch();
                if (stepsByGroup != null) {
                    steps = stepsByGroup;
                }
            }
        } else {
            steps = Step.find("deletedAt = null").fetch();
        }
        renderTemplate("admin/steps/index.html", active, steps, idInputName, nameInputName, groupDropdownName, groupDropdownOptions);
    }

    public static void stepsCreate(String positionNaNError, String emptyFieldsError) {
        String action = "Добавить шаг";
        String active = "steps";

        //input names
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        String nameInputName = "nameInput";
        String positionInputName = "positionInput";
        String colorPickerName = "colorPicker";
        String accessCheckboxName = "accessCheckbox";
        String accessCheckboxLabel = "Доступ после потверждения";

        renderTemplate("admin/steps/create.html", action, active, groupDropdownName, groupDropdownOptions, nameInputName, positionInputName, colorPickerName, accessCheckboxName, accessCheckboxLabel, positionNaNError, emptyFieldsError);
    }

    public static void newStep(String nameInput, String positionInput, String colorPicker, String groupDropdown, String accessCheckbox) {
        if (nameInput != null && !nameInput.equals("") && positionInput != null && !positionInput.equals("") && colorPicker != null && !colorPicker.equals("") && groupDropdown != null && !groupDropdown.equals("") && accessCheckbox != null && !accessCheckbox.equals("")) {
            //make sure position is number, otherwise render error
            Integer position = null;
            try {
                position = Integer.parseInt(positionInput);
            } catch (Exception e) {
                stepsCreate("Позиция должна иметь числовое значение", null);
            }

            Integer groupId = Integer.parseInt(groupDropdown);
            Group group = Group.findById(groupId);

            Step newStep = new Step(nameInput, position, colorPicker, group, accessCheckbox.contains("1"));
            newStep.save();
            steps(null, null, null);

        } else {
            stepsCreate(null, "Не все поля заполнены или заполнены некорректно");
        }
    }

    public static void stepsEdit(Integer stepId, String emptyFieldsError) {
        Step step = Step.findById(stepId);

        String action = "Сохранить изменения";
        String active = "steps";

        //input names
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        String nameInputName = "nameInput";
        String positionInputName = "positionInput";
        String colorPickerName = "colorPicker";
        String accessCheckboxName = "accessCheckbox";
        String accessCheckboxLabel = "Доступ после потверждения";

        renderTemplate("admin/steps/edit.html", step, action, active, groupDropdownName, groupDropdownOptions, nameInputName, positionInputName, colorPickerName, accessCheckboxName, accessCheckboxLabel, emptyFieldsError);
    }

    public static void updateStep(@Required Integer stepId, @Required String nameInput, @Required Integer positionInput, @Required String colorPicker, @Required String accessCheckbox) {
        if (Validation.hasErrors()) {
            stepsEdit(stepId, "Не все поля заполнены или заполнены некорректно");
        }

        Step step = Step.findById(stepId);
        step.name = nameInput;
        step.position = positionInput;
        step.color = colorPicker;
        step.isModerForward = accessCheckbox.contains("1");
        step.save();

        steps(null, null, null);
    }

    public static void removeStep(Integer stepId) {
        Step step = Step.findById(stepId);
        if (step == null) {
            accounts(1, null, null, null, null, null, null);
        }
        step.deletedAt = new Date();
        step.save();
        steps(null, null, null);
    }

    public static void lessons(String idInput, String nameInput, String groupDropdown) {
        List<Lesson> lessons = new ArrayList<Lesson>();
        String active = "lessons";

        //input names
        String idInputName = "idInput";
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();

        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }

        //handle filtering
        if (idInput != null && !idInput.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(idInput);
            } catch (Exception e) {

            }
            if (id != null) {
                Lesson lesson = Lesson.findById(id);
                 if (lesson != null && lesson.deletedAt == null) {
                     lessons.add(lesson);
                 }
            }
        } else if (nameInput != null && !nameInput.equals("")) {
            Lesson lesson = Lesson.find("name = ?1 and deletedAt = null and language = ?2", nameInput, language).first();
            if (lesson != null) {
                lessons.add(lesson);
            }
        } else if (groupDropdown != null && !groupDropdown.equals("")) {
            Group group = Group.findById(Integer.parseInt(groupDropdown));
            List<Lesson> lessonsByGroup = Lesson.find("group = ?1 and deletedAt = null and language = ?2", group, language).fetch();
            if (lessonsByGroup != null) {
                lessons = lessonsByGroup;
            }
        } else {
            lessons = Lesson.find("deletedAt = null and language = ?1", language).fetch();
        }
        renderTemplate("admin/lessons/index.html", active, lessons, idInputName, nameInputName, groupDropdownName, groupDropdownOptions);
    }

    public static void lessonsCreate(String emptyFieldsError) {

        //input names
        String groupDropdownName = "groupsDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        String stepDropdownName = "stepDropdown";
        String nameInputName = "nameInput";
        String descriptionInputName = "descriptionInput";
        String contentTinymceName = "contentTinymce";
        String pictureFileName = "pictureFile";
        String videoFileName = "videoFile";
        String subtitlesFileName = "subtitlesFile";
        String active = "lessons";

        renderTemplate("admin/lessons/create.html", active, groupDropdownName, groupDropdownOptions, stepDropdownName, nameInputName, descriptionInputName, contentTinymceName, pictureFileName, videoFileName, subtitlesFileName, emptyFieldsError);
    }

    public static void newLesson(@Required Integer groupsDropdown,
                                 @Required Integer stepDropdown,
                                 @Required String nameInput,
                                 String descriptionInput,
                                 String contentTinymce,
                                 File pictureFile,
                                 @Required File videoFile,
                                 File subtitlesFile) {
        if (Validation.hasErrors()) {
            renderTemplate("Не все поля заполнены или заполнены некорректно");
        } else {
            Group group = Group.findById(groupsDropdown);
            Step step = Step.findById(stepDropdown);
            File destFile1;
            File destFile2;
            File destFile3;
            String uploadedFileName1 = null;
            String uploadedFileName2;
            String uploadedFileName3 = null;

            //save picture if not null
            if (pictureFile != null) {
                String old = pictureFile.getName();
                int lastDot = old.lastIndexOf('.');
                String ext = old.substring(lastDot, old.length());

                uploadedFileName1 = new Date().getTime() + ext;
                destFile1 = new File(fileDirectory + lessonsPath + uploadedFileName1);
                destFile1.getParentFile().mkdirs();

                try {
                    destFile1.createNewFile();
                    byte[] bytes1 = ClientControllerHelper.readBytesFromFile(pictureFile);
                    ClientControllerHelper.writeBytesToFile(destFile1, bytes1);
                } catch (IOException e) {
                }
            }
            //save subtitles if not null
            if (subtitlesFile != null) {
                String old = subtitlesFile.getName();
                int lastDot = old.lastIndexOf('.');
                String ext = old.substring(lastDot, old.length());

                uploadedFileName3 = new Date().getTime() + ext;
                destFile3 = new File(fileDirectory + lessonsPath + uploadedFileName3);
                destFile3.getParentFile().mkdirs();

                try {
                    destFile3.createNewFile();
                    byte[] bytes3 = ClientControllerHelper.readBytesFromFile(subtitlesFile);
                    ClientControllerHelper.writeBytesToFile(destFile3, bytes3);
                } catch (IOException e) {
                }
            }

            String old = videoFile.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());

            //saving video
            uploadedFileName2 = new Date().getTime() + ext;
            destFile2 = new File(fileDirectory + lessonsPath + uploadedFileName2);
            destFile2.getParentFile().mkdirs();

            try {
                destFile2.createNewFile();
                byte[] bytes2 = ClientControllerHelper.readBytesFromFile(videoFile);
                ClientControllerHelper.writeBytesToFile(destFile2, bytes2);
            } catch (IOException e) {
            }

            Language language;
            if (play.i18n.Lang.get().equals("kz")) {
                language = Language.kz;
            } else {
                language = Language.ru;
            }
            String savePath1 = null;
            String savePath2 = null;
            String savePath3 = null;

            if (uploadedFileName1 != null) {
                savePath1 = lessonsPath + uploadedFileName1;
            }
            if (uploadedFileName2 != null) {
                savePath2 = lessonsPath + uploadedFileName2;
            }
            if (uploadedFileName3 != null) {
                savePath3 = lessonsPath + uploadedFileName3;
            }
            Lesson lesson = new Lesson(nameInput, descriptionInput, contentTinymce, savePath2, savePath1, savePath3, language, group, step);
            lesson.save();
            lessons(null, null, null);
        }
    }

    public static void lessonsEdit(Integer lessonId, String emptyFieldsError) {
        Lesson lesson = Lesson.findById(lessonId);

        //input names
        String groupDropdownName = "groupsDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        List<Step> stepDropdownInitialOptions = Step.find("group = ?1", lesson.group).fetch();
        String stepDropdownName = "stepDropdown";
        String nameInputName = "nameInput";
        String descriptionInputName = "descriptionInput";
        String contentTinymceName = "contentTinymce";
        String pictureFileName = "pictureFile";
        String videoFileName = "videoFile";
        String subtitlesFileName = "subtitlesFile";
        String active = "lessons";

        renderTemplate("admin/lessons/edit.html", active, lesson, groupDropdownName, groupDropdownOptions, stepDropdownInitialOptions, stepDropdownName, nameInputName, descriptionInputName, contentTinymceName, pictureFileName, videoFileName, subtitlesFileName, emptyFieldsError);
    }

    public static void updateLesson(Integer lessonId, @Required Integer groupsDropdown, @Required Integer stepDropdown, @Required String nameInput, String descriptionInput, String contentTinymce, File pictureFile, File videoFile, File subtitlesFile) {
        if (Validation.hasErrors()) {
            lessonsEdit(lessonId, "Не все поля заполнены или заполнены некорректно");
        }

        Group group = Group.findById(groupsDropdown);
        Step step = Step.findById(stepDropdown);

        File destFile;
        Lesson lesson = Lesson.findById(lessonId);
        lesson.group = group;
        lesson.step = step;
        lesson.name = nameInput;
        lesson.description = descriptionInput;
        lesson.content = contentTinymce;

        String uploadedFileName1 = null;
        String uploadedFileName2 = null;
        String uploadedFileName3 = null;

        if (pictureFile != null) {
            //renaming video file
            String old = pictureFile.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());

            //saving video
            uploadedFileName1 = new Date().getTime() + ext;
            destFile = new File(fileDirectory + lessonsPath + uploadedFileName1);
            destFile.getParentFile().mkdirs();
            try {
                destFile.createNewFile();
                byte[] bytes = ClientControllerHelper.readBytesFromFile(pictureFile);
                ClientControllerHelper.writeBytesToFile(destFile, bytes);
            } catch (IOException e) {
            }
        }

        if (videoFile != null) {
            //renaming video file
            String old = videoFile.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());

            //saving video
            uploadedFileName2 = new Date().getTime() + ext;
            destFile = new File(fileDirectory + lessonsPath + uploadedFileName2);
            destFile.getParentFile().mkdirs();
            try {
                destFile.createNewFile();
                byte[] bytes = ClientControllerHelper.readBytesFromFile(videoFile);
                ClientControllerHelper.writeBytesToFile(destFile, bytes);
            } catch (IOException e) {
            }
        }
        if (subtitlesFile != null) {
            //renaming video file
            String old = subtitlesFile.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());

            //saving video
            uploadedFileName3 = new Date().getTime() + ext;
            destFile = new File(fileDirectory + lessonsPath + uploadedFileName3);
            destFile.getParentFile().mkdirs();
            try {
                destFile.createNewFile();
                byte[] bytes = ClientControllerHelper.readBytesFromFile(subtitlesFile);
                ClientControllerHelper.writeBytesToFile(destFile, bytes);
            } catch (IOException e) {
            }
        }

        //save pathes for files
        String savePath1;
        String savePath2;
        String savePath3;

        if (uploadedFileName1 != null) {
            savePath1 = lessonsPath + uploadedFileName1;
            lesson.picture = savePath1;
        }
        if (uploadedFileName2 != null) {
            savePath2 = lessonsPath + uploadedFileName2;
            lesson.video = savePath2;
        }
        if (uploadedFileName3 != null) {
            savePath3 = lessonsPath + uploadedFileName3;
            lesson.subtitles = savePath3;
        }
        lesson.save();
        lessons(null, null, null);
    }

    public static void removeLesson(Integer lessonId) {
        Lesson lesson = Lesson.findById(lessonId);
        lesson.deletedAt = new Date();
        lesson.save();
        lessons(null, null, null);
    }

    public static void documents(Integer idInput, String nameInput, Integer groupDropdown) {
        if (Validation.hasErrors()) {
            documents(null, null, null);
        }
        List<Document> documents = new ArrayList<Document>();
        //input names
        String idInputName = "idInput";
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";

        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }

        List<Group> groupDropdownOptions = Group.findAll();
        if (idInput != null && !idInput.equals("")) {
            Document doc = Document.findById(idInput);
            if (doc != null) {
                documents.add(doc);
            }
        } else if (nameInput != null && !nameInput.equals("")) {
            Document doc = Document.find("name = ?1 and language = ?2", nameInput, language).first();
            if (doc != null) {
                documents.add(doc);
            }
        } else if (groupDropdown != null && !groupDropdown.equals("")) {
            Group group = Group.findById(groupDropdown);
            documents = Document.find("group = ?1 and language = ?2", group, language).fetch();
        } else {
            documents = Document.find("language = ?1", language).fetch();
        }
        String active = "documents";
        renderTemplate("admin/documents/index.html", active, documents, idInputName, nameInputName, groupDropdownName, groupDropdownOptions);
    }


    public static void documentsCreate(String emptyFieldsError) {
        String active = "documents";
        //input names, options
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        String stepDropdownName = "stepDropdown";
        String fileName = "file";
        renderTemplate("admin/documents/create.html", active, nameInputName, groupDropdownName, groupDropdownOptions, stepDropdownName, fileName, emptyFieldsError);
    }

    public static void newDocument(@Required String nameInput,
                                   @Required Integer groupDropdown,
                                   @Required Integer stepDropdown,
                                   @Required File file) {
        if (Validation.hasErrors()) {
            documentsCreate("Не все поля заполнены или заполнены некорректно");
        }
        //saving video
        String uploadedFileName;
        File destFile;

        //name proccessing
        String old = file.getName();
        int lastDot = old.lastIndexOf('.');
        String ext = old.substring(lastDot, old.length());
        uploadedFileName = new Date().getTime() + ext;
        destFile = new File(fileDirectory + documentsPath + uploadedFileName);
        destFile.getParentFile().mkdirs();
        try {
            destFile.createNewFile();
            byte[] bytes = ClientControllerHelper.readBytesFromFile(file);
            ClientControllerHelper.writeBytesToFile(destFile, bytes);
        } catch (IOException e) {
        }
        String pathName = null;
        if (uploadedFileName != null) {
            pathName = documentsPath + uploadedFileName;
        }
        //language
        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }
        //group
        Group group = Group.findById(groupDropdown);
        //step
        Step step = Step.findById(stepDropdown);
        Document document = new Document(nameInput, pathName, language, group, step);
        document.save();
        documents(null, null, null);
    }

    public static void documentsEdit(Integer documentId, String emptyFieldsError) {
        String active = "documents";
        Document document = Document.findById(documentId);
        //input names, options
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        List<Step> stepDropdownInitialOptions = Step.find("group = ?1", document.group).fetch();
        String stepDropdownName = "stepDropdown";
        String fileName = "file";

        renderTemplate("admin/documents/edit.html", document, stepDropdownInitialOptions,active, nameInputName, groupDropdownName, groupDropdownOptions, stepDropdownName, fileName, emptyFieldsError);
    }

    public static void updateDocument(Integer documentId, @Required String nameInput, @Required Integer groupDropdown, @Required Integer stepDropdown, File file) {
        if (Validation.hasErrors()) {
            documentsEdit(documentId, "Не все поля заполнены или заполнены неккоректно");
        }
        Document document = Document.findById(documentId);
        Group group = Group.findById(groupDropdown);
        Step step = Step.findById(stepDropdown);
        document.name = nameInput;
        document.group = group;
        document.step = step;
        if (file != null) {
            //saving video
            String uploadedFileName;
            File destFile;
            //name proccessing
            String old = file.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());
            uploadedFileName = new Date().getTime() + ext;
            destFile = new File(fileDirectory + documentsPath + uploadedFileName);
            destFile.getParentFile().mkdirs();
            try {
                destFile.createNewFile();
                byte[] bytes = ClientControllerHelper.readBytesFromFile(file);
                ClientControllerHelper.writeBytesToFile(destFile, bytes);
            } catch (IOException e) {
            }
            String pathName = null;
            if (uploadedFileName != null) {
                pathName = documentsPath + uploadedFileName;
            }
            document.file = pathName;
        }
        document.save();
        documents(null, null, null);
    }

    public static void removeDocument(Integer documentId) {
        Document document = Document.findById(documentId);
        document.delete();
        documents(null, null, null);
    }

    public static void quizzes(Integer idInput, String nameInput, Integer groupDropdown) {
        List<Quiz> quizzes = new ArrayList<Quiz>();

        //input names
        String idInputName = "idInput";
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();

        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }

        if (idInput != null && !idInput.equals("")) {
            Quiz quiz = Quiz.findById(idInput);
            if (quiz != null) {
                quizzes.add(quiz);
            }
        } else if (nameInput != null && !nameInput.equals("")) {
            Quiz quiz = Quiz.find("name = ?1 and language = ?2", nameInput, language).first();
            if (quiz != null) {
                quizzes.add(quiz);
            }
        } else if (groupDropdown != null && !groupDropdown.equals("")) {
            Group group = Group.findById(groupDropdown);
            quizzes = Quiz.find("group = ?1 and language = ?2", group, language).fetch();
        } else {
            quizzes = Quiz.find("language = ?1", language).fetch();
        }
        String active = "quizzes";
        renderTemplate("admin/quizzes/index.html", active, quizzes, idInputName, nameInputName, groupDropdownName, groupDropdownOptions);
    }

    public static void quizzesCreate(String emptyFieldsError) {

        //input names, options
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        String passvalInputName = "passvalInput";
        String descriptionTextName = "descriptionText";
        String contentTinymceName = "contentTinymce";
        List<Group> groupDropdownOptions = Group.findAll();
        String stepDropdownName = "stepDropdown";

        String active = "quizzes";

        renderTemplate("admin/quizzes/create.html", active, nameInputName, groupDropdownName, passvalInputName, contentTinymceName, descriptionTextName, groupDropdownOptions, stepDropdownName, emptyFieldsError);
    }

    public static void newQuiz(@Required String nameInput,
                               @Required Integer groupDropdown,
                               @Required Integer stepDropdown,
                               @Required Integer passvalInput,
                               String descriptionText,
                               String contentTinymce,
                               List<String> questions,
                               List<String> descriptions,
                               @As("answers[][]:") List<List<String>> answers,
                               @As("isright[][]:") List<List<String>> isright) {
        if(Validation.hasErrors()) {
            quizzesCreate("Не все поля заполнены или заполнены некорректно");
        }
        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }
        Group group = Group.findById(groupDropdown);
        Step step = Step.findById(stepDropdown);
        Quiz quiz = new Quiz(nameInput, descriptionText, contentTinymce, language, passvalInput, group, step);
        quiz.save();
        for (int i = 0; i < questions.size(); i++) {
            QuizQuestion quizQuestion = new QuizQuestion(quiz, questions.get(i), descriptions.get(i), i);
            quizQuestion.save();
            for (int j = 0; j < answers.get(i).size(); j++) {
                QuizAnswer quizAnswer = new QuizAnswer(quiz, quizQuestion, answers.get(i).get(j), isright.get(i).get(j).contains("1"));
                quizAnswer.save();
            }
        }
        quizzes(null, null, null);
    }

    public static void quizzesEdit(Integer quizId, String emptyFieldsError) {
        Quiz quiz = Quiz.findById(quizId);

        //input names, options
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        String passvalInputName = "passvalInput";
        String descriptionTextName = "descriptionText";
        String contentTinymceName = "contentTinymce";
        List<Group> groupDropdownOptions = Group.findAll();
        String stepDropdownName = "stepDropdown";
        List<Step> stepDropdownOptions = Step.findAll();

        String active = "quizzes";
        renderTemplate("admin/quizzes/edit.html", quiz, nameInputName, groupDropdownName, passvalInputName, descriptionTextName, contentTinymceName, groupDropdownOptions, stepDropdownName, stepDropdownOptions, active);
    }

    public static void updateQuiz(Integer quizId,
                                  @Required String nameInput,
                                  @Required Integer groupDropdown,
                                  @Required Integer stepDropdown,
                                  @Required Integer passvalInput,
                                  String descriptionText,
                                  String contentTinymce,
                                  List<String> questions,
                                  List<String> descriptions,
                                  @As("answers[][]:") List<List<String>> answers,
                                  @As("isright[][]:") List<List<String>> isright) {
        if (Validation.hasErrors()) {
            quizzesCreate("Не все поля заполнены или заполнены некорректно");
        }
        answers.remove(null);
        Quiz quiz = Quiz.findById(quizId);
        Group group = Group.findById(groupDropdown);
        Step step = Step.findById(stepDropdown);

        quiz.group = group;
        quiz.step = step;
        quiz.name = nameInput;
        quiz.description = descriptionText;
        quiz.content = contentTinymce;
        quiz.updatedAt = new Date();
        quiz.scoreToPass = passvalInput;
        quiz.save();
        //deleting questions and answers
        for (QuizQuestion question: quiz.quizQuestions) {
            question.delete();
        }
        for (int i = 0; i < questions.size(); i++) {
            QuizQuestion quizQuestion = new QuizQuestion(quiz, questions.get(i), descriptions.get(i), i);
            quizQuestion.save();
            for (int j = 0; j < answers.get(i).size(); j++) {
                QuizAnswer quizAnswer = new QuizAnswer(quiz, quizQuestion, answers.get(i).get(j), isright.get(i).get(j).contains("1"));
                quizAnswer.save();
            }
        }
        List<QuizScore> unfinishedQuizScores = QuizScore.find("quiz = ?1 and isFinished = false", quiz).fetch();
        for (QuizScore quizScore: unfinishedQuizScores) {
            quizScore.delete();
        }
        quizzes(null, null, null);
    }

    public static void removeQuiz(Integer quizId) {
        Quiz quiz = Quiz.findById(quizId);
        quiz.delete();
        quizzes(null, null, null);
    }

    public static void news(Integer idInput, String nameInput, Integer groupDropdown) {
        List<News> newsList = new ArrayList<News>();
        String active = "news";

        //input names
        String idInputName = "idInput";
        String nameInputName = "nameInput";
        String groupDropdownName = "groupDropdown";
        List<Group> groupDropdownOptions = Group.findAll();

        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }

        if (idInput != null) {
            News news = News.findById(idInput);
            if (news != null && news.deletedAt == null) {
                newsList.add(news);
            }
        } else if (nameInput != null && !nameInput.trim().equals("")) {
            News news = News.find("name = ?1 and deletedAt = null and language = ?2", nameInput, language).first();
            newsList.add(news);
        } else if (groupDropdown != null) {
            Group group = Group.findById(groupDropdown);
            newsList = News.find("group = ?1 and deletedAt = null and language = ?2", group, language).fetch();
        } else {
            newsList = News.find("deletedAt = null and language =?1", language).fetch();
        }
        renderTemplate("admin/news/index.html", newsList, active, idInputName, nameInput, groupDropdown);
    }

    public static void newsCreate(String emptyFieldsError) {
        //input names
        String groupDropdownName = "groupsDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        String newsTypeDropdownName = "newsTypeDropdown";
        List<NewsType> newsTypeDropdownOptions = NewsType.findAll();
        String nameInputName = "nameInput";
        String contentTinymceName = "contentTinymce";
        String descriptionInputName = "descriptionInput";
        String pictureFileName = "pictureFile";
        String videoFileName = "videoFile";
        String documentRuFileName = "documentRuFile";
        String documentKzFileName = "documentKzFile";
        String active = "news";

        renderTemplate("admin/news/create.html", groupDropdownName, groupDropdownOptions, nameInputName, contentTinymceName, descriptionInputName, pictureFileName, videoFileName, documentRuFileName, documentKzFileName, active, emptyFieldsError, newsTypeDropdownName, newsTypeDropdownOptions);
    }


    public static void newNews( Integer groupsDropdown,
                                @Required Integer newsTypeDropdown,
                                @Required String nameInput,
                                @Required String contentTinymce,
                                String descriptionInput,
                                File pictureFile,
                                File videoFile,
                                File documentRuFile,
                                File documentKzFile,
                                Boolean openDocumentOnView) {
        if(Validation.hasErrors()) {
            newsCreate("Не все поля заполнены или заполнены некорректно");
        }
        NewsType newsType = NewsType.findById(newsTypeDropdown);
        String uploadedVideoName = ClientControllerHelper.storeNewsResource(videoFile);
        String uploadedPictureName = ClientControllerHelper.storeNewsResource(pictureFile);
        String uploadedDocumentRuName = ClientControllerHelper.storeNewsResource(documentRuFile);
        String uploadedDocumentKzName = ClientControllerHelper.storeNewsResource(documentKzFile);
        String videoDBPath = null;
        String pictureDBPath = null;
        String documentRuDBPath = null;
        String documentKzDBPath = null;

        if (uploadedVideoName != null) {
            videoDBPath = newsPath + uploadedVideoName;
        }

        if (uploadedPictureName != null) {
            pictureDBPath = newsPath + uploadedPictureName;
        }

        if (uploadedDocumentRuName != null) {
            documentRuDBPath = newsPath + uploadedDocumentRuName;
        }

        if (uploadedDocumentKzName != null) {
            documentKzDBPath = newsPath + uploadedDocumentKzName;
        }

        Language language;

        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }

        Group group = Group.findById(groupsDropdown);

        News newNews = new News(nameInput, contentTinymce, descriptionInput, newsType, videoDBPath, pictureDBPath, language, group);
        newNews.documentKz = documentKzDBPath;
        newNews.documentRu = documentRuDBPath;
        newNews.openDocumentOnView = openDocumentOnView;
        newNews.save();

        news(null, null, null);
    }

    public static void editNews(Integer newsId) {
        News news = News.findById(newsId);

        //input names
        String groupDropdownName = "groupsDropdown";
        List<Group> groupDropdownOptions = Group.findAll();
        if (news.group != null) {
            groupDropdownOptions.add(0, new Group("Общая", ""));
        }
        String newsTypeDropdownName = "newsTypeDropdown";
        List<NewsType> newsTypeDropdownOptions = NewsType.findAll();
        String nameInputName = "nameInput";
        String contentTinymceName = "contentTinymce";
        String descriptionInputName = "descriptionInput";
        String pictureFileName = "pictureFile";
        String videoFileName = "videoFile";
        String documentRuFileName = "documentRuFile";
        String documentKzFileName = "documentKzFile";
        String active = "news";

        renderTemplate("admin/news/edit.html", news, groupDropdownName, groupDropdownOptions,
                newsTypeDropdownName, newsTypeDropdownOptions, nameInputName, contentTinymceName,
                pictureFileName, videoFileName, active, descriptionInputName, documentRuFileName, documentKzFileName);
    }

    public static void updateNews(Integer newsId,
                                  @Required Integer groupsDropdown,
                                  @Required Integer newsTypeDropdown,
                                  @Required String nameInput,
                                  @Required String contentTinymce,
                                  String descriptionInput,
                                  File pictureFile,
                                  File videoFile,
                                  File documentRuFile,
                                  File documentKzFile,
                                  Boolean openDocumentOnView) {
        News news = News.findById(newsId);

        String uploadedVideoName = ClientControllerHelper.storeNewsResource(videoFile);
        String uploadedPictureName = ClientControllerHelper.storeNewsResource(pictureFile);
        String uploadedDocumentRuName = ClientControllerHelper.storeNewsResource(documentRuFile);
        String uploadedDocumentKzName = ClientControllerHelper.storeNewsResource(documentKzFile);
        String videoDBPath = null;
        String pictureDBPath = null;
        String documentRuDBPath = null;
        String documentKzDBPath = null;

        if (uploadedVideoName != null) {
            videoDBPath = newsPath + uploadedVideoName;
        }

        if (uploadedPictureName != null) {
            pictureDBPath = newsPath + uploadedPictureName;
        }

        if (uploadedDocumentRuName != null) {
            documentRuDBPath = newsPath + uploadedDocumentRuName;
        }

        if (uploadedDocumentKzName != null) {
            documentKzDBPath = newsPath + uploadedDocumentKzName;
        }

        Group group = null;
        if (groupsDropdown != null) {
            group = Group.findById(groupsDropdown);
        }
        NewsType newsType = NewsType.findById(newsTypeDropdown);

        news.name = nameInput;
        news.content = contentTinymce;
        news.group = group;
        news.newsType = newsType;
        news.updatedAt = new Date();
        if (documentKzDBPath != null) {
            news.documentKz = documentKzDBPath;
        }
        if (documentRuDBPath != null) {
            news.documentRu = documentRuDBPath;
        }
        news.openDocumentOnView = openDocumentOnView;
        if (videoDBPath != null) {
            news.video = videoDBPath;
        }
        if (pictureDBPath != null) {
            news.picture = pictureDBPath;
        }
        news.description = descriptionInput;
        news.save();

        news(null, null, null);
    }

    public static void removeNews(Integer newId) {
        News news = News.findById(newId);
        news.deletedAt = new Date();
        news.save();
        news(null, null, null);
    }

    public static void supervisors(String idFilter, String loginFilter, String fullnameFilter) {
        List<Supervisor> supervisors = new ArrayList();

        //input names
        String idInputName = "idFilter";
        String loginInputName = "loginFilter";
        String fullnameInputName = "fullnameFilter";

        //handle filtering
        if (idFilter != null && !idFilter.equals("")) {
            Integer id = null;
            try {
                id = Integer.parseInt(idFilter);
            } catch (Exception e) {
            }
            if (id != null) {
                Supervisor supervisor = Supervisor.findById(id);
                if (supervisor != null) {
                    supervisors.add(supervisor);
                }
            }
        } else if (loginFilter != null && !loginFilter.trim().equals("")) {
            List<Supervisor> supervisors2 = Supervisor.find("login like ?1", "%" + loginFilter.trim() + "%").fetch();
            supervisors.addAll(supervisors2);
        } else if (fullnameFilter != null && !fullnameFilter.trim().equals("")) {
            List<Supervisor> supervisors2 = Supervisor.find("fullname like ?1", "%" + fullnameFilter.trim() + "%").fetch();
            supervisors.addAll(supervisors2);
        } else {
            supervisors = Supervisor.findAll();
        }
        String active = "supervisors";
        renderTemplate("admin/supervisors/index.html", active, supervisors, idFilter, loginFilter, fullnameFilter, idInputName, loginInputName, fullnameInputName);
    }

    public static void supervisorsForm(Integer id) {
        Supervisor supervisor = null;
        if (id != null) {
            supervisor = Supervisor.findById(id);
        }
        renderArgs.put("emptyFieldsError", flash.get("emptyFieldsError"));
        renderArgs.put("duplicateLoginError", flash.get("duplicateLoginError"));
        //input names and other
        String active = "supervisors";
        String action = "Добавить супервайзера";
        String loginInputName = "login";
        String fullnameInputName = "fullname";
        String passwordInputName = "password";
        String typeDropdownName = "type";
        List<String> typeDropdownOptions = new ArrayList<String>();
        typeDropdownOptions.add("Республиканский");
        typeDropdownOptions.add("Областной");
        typeDropdownOptions.add("Районный");
        String regionDropdownName = "regionId";
        String provinceDropdownName = "provinceId";


        renderTemplate("admin/supervisors/create.html", supervisor, active, action, loginInputName, passwordInputName, fullnameInputName, typeDropdownOptions, typeDropdownName, regionDropdownName, provinceDropdownName);
    }

    public static void saveSupervisor(Integer id, @Required String login, String password, String fullname, @Required Integer type, Integer regionId, Integer provinceId) {
        Supervisor supervisor = null;
        if (Validation.hasErrors()) {
            flash.put("emptyFieldsError", "Не все поля заполнены или заполнены неверно.");
            supervisorsForm(id);
        } else {
            if (id != null) {
                supervisor = Supervisor.findById(id);
            } else {
                supervisor = new Supervisor();
            }
            if ((type != 0 && type != 1 && type != 2) || (type == 1 && regionId == null && supervisor.region == null) || (type == 2 && provinceId == null && supervisor.province == null)) {
                flash.put("emptyFieldsError", "Не все поля заполнены или заполнены неверно");
                supervisorsForm(id);
            }
        }
        Location province = null;
        Location region = null;

        if (provinceId != null) {
            province = Location.findById(provinceId);
        }

        if (regionId != null) {
            region = Location.findById(regionId);
        }
        supervisor.login = login;
        if ((password == null || password.trim().equals(""))) {
            if (supervisor.id == null) {
                flash.put("emptyFieldsError", "Заполните пароль");
                supervisorsForm(null);
            }
        } else {
            supervisor.password = BCrypt.hashpw(password, BCrypt.gensalt());
        }
        supervisor.fullname = fullname;
        supervisor.type = Supervisor.Type.values()[type];
        if (supervisor.type.equals(Supervisor.Type.republic)) {
            supervisor.province = null;
            supervisor.region = null;
        } else if (supervisor.type.equals(Supervisor.Type.region)) {
            supervisor.province = null;
            supervisor.region = region == null ? supervisor.region : region;
        } else if (supervisor.type.equals(Supervisor.Type.province)) {
            supervisor.region = region == null ? supervisor.region : region;
            supervisor.province = province == null ? supervisor.province : province;
        }
        try {
            supervisor.save();
        } catch (Exception e) {
            flash.put("duplicateLoginError", "Супервайзер с таким логином уже существует");
            supervisorsForm(null);
        }
        supervisors(null, null, null);
    }

    public static void removeSupervisor(Integer id) {
        Supervisor supervisor = null;
        if (id != null) {
            supervisor = Supervisor.findById(id);
        }
        if (supervisor != null) {
            supervisor.delete();
        }
        supervisors(null, null, null);
    }

    public static void questions() {
        renderArgs.put("active", "questions");
        List<UserQuestion> userQuestions =
                JPA.em().createQuery("Select f from UserQuestion f", UserQuestion.class).getResultList();
            renderTemplate("admin/questions/index.html", userQuestions);
        }

    public static void userQuestionsForm(Integer id) {
        UserQuestion userQuestion = null;
        if (id != null) {
            userQuestion = UserQuestion.findById(id);
        }
        renderArgs.put("emptyFieldsError", flash.get("emptyFieldsError"));

        //input names and other
        String active = "userQuestions";
        String action = "Сохранить";
        String questionKzName = "questionKz";
        String answerKzName = "answerKz";
        String questionRuName = "questionRu";
        String answerRuName = "answerRu";
        String questionEnName = "questionEn";
        String answerEnName = "answerEn";

        renderTemplate("admin/questions/create.html",
                userQuestion, active, action, questionKzName, answerKzName,
                questionRuName, answerRuName, questionEnName, answerEnName);
    }

    public static void removeUserQuestion(Integer id) {
        UserQuestion userQuestion = null;
        if (id != null) {
            userQuestion = UserQuestion.findById(id);
        }
        if (userQuestion != null) {
            userQuestion.delete();
        }
        questions();
    }

    public static void updateUserQuestion(Integer id, String questionKz, String answerKz,
                                          String questionRu, String answerRu,
                                          String questionEn, String answerEn,
                                          boolean isFaq, boolean isActive, Integer accountId) {
        UserQuestion userQuestion = new UserQuestion();
        if (id != null) {
            userQuestion = UserQuestion.findById(id);
        }
        userQuestion.questionKz = questionKz;
        userQuestion.answerKz = answerKz;
        userQuestion.questionRu = questionRu;
        userQuestion.answerRu = answerRu;
        userQuestion.questionEn = questionEn;
        userQuestion.answerEn = answerEn;
        userQuestion.isFaq = isFaq;
        userQuestion.isActive = isActive;
        userQuestion.save();
        questions();
    }

    public static void offlineResults() {
        renderArgs.put("error", flash.get("error"));
        renderArgs.put("active", "offline");
        renderTemplate("admin/offline/index.html");
    }

    public static void uploadOffline(String login, File file) {
        if (request.method.equals("GET")) {
            offlineResults();
        }
        String fileContent = null;
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            fileContent = new String(bytes, "UTF-8");
        } catch (Exception e) {
            flash.put("error", "Загрузите файл");
            offlineResults();
        }
        if (fileContent == null) {
            flash.put("error", "Ошибка с чтением файла");
            offlineResults();
        }
        OfflineStudentsArrayDto dto = null;
        try {
            dto = new Gson().fromJson(fileContent, OfflineStudentsArrayDto.class);
        } catch (JsonSyntaxException e) {
            flash.put("error", "Неправильный формат файла");
            offlineResults();
        }
        if (login == null || login.trim().equals("")) {
            flash.put("error", "Введите логин психолога");
            offlineResults();
        }
        Account account = Account.find("username = ?1", login.trim()).first();
        if (account == null || !account.group.key.equals("psychologist")) {
            flash.put("error", "Психолог не найден");
            offlineResults();
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        List<Student> existingStudents = new ArrayList<>();
        int savedNumber = 0;
        for (OfflineStudentsArrayDto.OfflineStudentDto studentDto: dto.students) {
            String code = studentDto.code.replaceAll("-", "");
            if (!StringUtils.isNumeric(code)) {
                continue;
            } else {
                try {
                    Student existingStudent = Student.find("code = ?1", code).first();
                    if (existingStudent != null) {
                        existingStudents.add(existingStudent);
                        continue;
                    }
                    Student newStudent = new Student();
                    newStudent.code = code;
                    newStudent.status = Student.Status.valueOf(studentDto.status);
                    newStudent.createdAt = format.parse(studentDto.createdDate);
                    newStudent.updatedAt = new Date();
                    newStudent.offline = true;
                    newStudent.gender = Gender.valueOf(studentDto.gender);
                    newStudent.creator = account;
                    newStudent.school = account.school;
                    newStudent.save();
                    savedNumber++;
                    SurveyResult newResult = null;
                    if (newStudent.status.equals(Student.Status.interrupted)) {
                        if (studentDto.interrupted == null || studentDto.interrupted.question == null || studentDto.interrupted.state == null) {
                            newStudent.status = Student.Status.created;
                        } else {
                            newStudent.interruptedAt = studentDto.interrupted.question;
                            newStudent.stateAtInterruption = studentDto.interrupted.state;
                        }
                        newStudent.save();
                    } else if (newStudent.status.equals(Student.Status.finished)) {
                        try {
                            newResult = SurveyUtils.analyzeResults(studentDto.surveyResult);
                            newResult.student = newStudent;
                            newResult.createdAt = format.parse(studentDto.surveyResult.date);
                            newResult.allQuestionsJSON = new Gson().toJson(studentDto.surveyResult);
                            newResult.save();
                        } catch (Exception e) {
                            newStudent.status = Student.Status.created;
                            newStudent.save();
                        }
                    }
                    if (!newStudent.status.equals(Student.Status.created) && studentDto.interviewResult != null) {
                        InterviewResult newInterview = new InterviewResult();
                        newInterview.verdict = InterviewResult.Verdict.valueOf(studentDto.interviewResult.verdict);
                        newInterview.text = studentDto.interviewResult.text;
                        newInterview.updatedAt = format.parse(studentDto.interviewResult.updatedDate);
                        newInterview.student = newStudent;
                        newInterview.risk = newStudent.status.equals(Student.Status.interrupted) ? SurveyResult.Risk.high : newResult == null ? null : newResult.risk;
                        newInterview.save();
                        if (!newStudent.status.equals(Student.Status.interrupted) && newResult != null) {
                            SurveyResult surveyResult = newResult;
                            surveyResult.interviewResult = newInterview;
                            surveyResult.save();
                        }
                    }
                } catch (Exception e) {
                    Logger.info("Error with saving a student : " + code);
                    continue;
                }
            }
        }
        renderArgs.put("saved", savedNumber);
        renderArgs.put("existing", existingStudents);
        renderArgs.put("active", "offline");
        renderTemplate("admin/offline/index.html");
    }
}
