package controllers;

import dto.RiskDataDto;
import dto.SupervisorDto;
import enums.Gender;
import models.*;
import play.mvc.Before;
import play.mvc.Controller;
import utils.BCrypt;

import java.util.ArrayList;
import java.util.List;

public class MonitoringController extends Controller {

    @Before(unless = {"index", "login", "logout"})
    public static void checkSecurity() {
        if (session.get("supervisorLogged") == null) {
            response.status = 401;
            renderText("login");
        }
        Supervisor supervisor = Supervisor.findById(Integer.valueOf(session.get("supervisorLogged")));
        if (supervisor == null) {
            response.status = 401;
            renderText("login");
        } else {
            renderArgs.put("s.type", supervisor.type);
            if (supervisor.type.equals(Supervisor.Type.region)) {
                renderArgs.put("s.region", supervisor.region);
            } else if (supervisor.type.equals(Supervisor.Type.province)) {
                renderArgs.put("s.region", supervisor.region);
                renderArgs.put("s.province", supervisor.province);
            }
        }
    }

    public static void index() {
        render();
    }

    public static void login(String login, String password) {
        Supervisor supervisor = Supervisor.find("login = ?1", login).first();
        if (supervisor != null) {
            if (BCrypt.checkpw(password, supervisor.password)) {
                session.put("supervisorLogged", supervisor.id);
                renderJSON(new SupervisorDto(supervisor));
            }
        }
        response.status = 401;
        renderText("denied");
    }

    public static void validate() {
        Supervisor supervisor = Supervisor.findById(Integer.valueOf(session.get("supervisorLogged")));
        renderJSON(new SupervisorDto(supervisor));
    }

    public static void updateRisks() {
        List<SurveyResult> surveyResults = SurveyResult.findAll();
        for (SurveyResult surveyResult: surveyResults) {
            surveyResult.risk = surveyResult.risk();
            surveyResult.save();
        }
        renderText("done");
    }

    public static void republic() {
        if (!renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.republic)) {
            renderText("denied");
        }
        RiskDataDto riskDataDto = new RiskDataDto();
        Long maleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.m);
        Long femaleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.f);
        riskDataDto.id = 0;
        riskDataDto.name = "Казахстан";
        riskDataDto.maleCount = maleCount;
        riskDataDto.femaleCount = femaleCount;
        riskDataDto.sum = maleCount + femaleCount;
        riskDataDto.children = new ArrayList<RiskDataDto>();
        riskDataDto.type = "country";
        Location kazakhstan = Location.findById(1);
        List<Location> locations = Location.find("parent0 = ?1 and parent1 = null and parent2 = null", kazakhstan).fetch();
        for (Location location: locations) {
            riskDataDto.children.add(fillRegion(location));
        }
        renderJSON(riskDataDto);
    }

    public static void region(Integer id) {
        if (id == null) {
            renderText("error");
        }
        if (!renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.republic)) {
            if (!renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.region)) {
                renderText("denied");
            }
            if (!renderArgs.get("s.region", Location.class).id.equals(id)) {
                renderText("denied");
            }
        }
        Location location = Location.findById(id);
        if (location == null || location.parent0 == null || location.parent1 != null || location.parent2 != null) {
            renderText("denied");
        }
        RiskDataDto riskDataDto = fillRegion(location);
        riskDataDto.children = new ArrayList<RiskDataDto>();
        Location kazakhstan = Location.findById(1);
        List<Location> locations = Location.find("parent0 = ?1 and parent1 = ?2 and parent2 = null", location, kazakhstan).fetch();
        for (Location eachChild: locations) {
            riskDataDto.children.add(fillProvince(eachChild));
        }
        if (riskDataDto.children.size() == 0) {
            //LOAD SCHOOLS IT IS ASTANA OR ALMATY
            List<School> schools = School.find("region = ?1", location).fetch();
            for (School eachChild: schools) {
                riskDataDto.children.add(fillSchool(eachChild));
            }
        }
        riskDataDto.type = "region";
        renderJSON(riskDataDto);
    }

    public static void province(Integer id) {
        if (id == null) {
            renderText("error");
        }
        Location location = Location.findById(id);
        if (location == null || location.parent0 == null || location.parent1 == null || location.parent2 != null) {
            renderText("denied");
        }
        if (!renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.republic)) {
            if (renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.region)) {
                if (!renderArgs.get("s.region", Location.class).id.equals(location.parent0.id)) {
                    renderText("denied");
                }
            } else if (renderArgs.get("s.type", Supervisor.Type.class).equals(Supervisor.Type.province)) {
                if (!renderArgs.get("s.province", Location.class).id.equals(location.id)) {
                    renderText("denied");
                }
            }
        }
        RiskDataDto riskDataDto = fillProvince(location);
        riskDataDto.children = new ArrayList<RiskDataDto>();
        List<School> schools = School.find("province = ?1", location).fetch();
        riskDataDto.type = "province";
        for (School eachChild: schools) {
            riskDataDto.children.add(fillSchool(eachChild));
        }
        renderJSON(riskDataDto);
    }

    private static RiskDataDto fillSchool(School school) {
        RiskDataDto result = new RiskDataDto();
        result.id = school.id;
        result.name = school.name;
        Long maleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.m, school);
        Long femaleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.f, school);
        result.maleCount = maleCount;
        result.femaleCount = femaleCount;
        result.sum = maleCount + femaleCount;
        result.type = "school";
        return result;
    }

    private static RiskDataDto fillProvince(Location location) {
        RiskDataDto result = new RiskDataDto();
        result.id = location.id;
        result.name = location.name;
        Long maleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school.province = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.m, location);
        Long femaleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school.province = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.f, location);
        result.maleCount = maleCount;
        result.femaleCount = femaleCount;
        result.sum = maleCount + femaleCount;
        result.type = "province";
        return result;
    }

    private static RiskDataDto fillRegion(Location location) {
        RiskDataDto result = new RiskDataDto();
        result.id = location.id;
        result.name = location.name;
        Long maleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school.region = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.m, location);
        Long femaleCount = InterviewResult.count("verdict = ?1 and (risk = ?2 or risk = ?3) and student.gender = ?4 and student.school.region = ?5",
                InterviewResult.Verdict.approved, SurveyResult.Risk.high,
                SurveyResult.Risk.presents, Gender.f, location);
        result.maleCount = maleCount;
        result.femaleCount = femaleCount;
        result.sum = maleCount + femaleCount;
        result.type = "region";
        return result;
    }

    public static void logout() {
        session.remove("supervisorLogged");
        renderText("success");
    }
}
