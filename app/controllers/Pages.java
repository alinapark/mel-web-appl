package controllers;

import enums.Language;
import models.*;
import play.Play;
import play.db.jpa.JPA;
import play.i18n.Lang;

import java.io.File;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by sagynysh on 5/17/17.n
 */
public class Pages extends SecureController {

    final static String FILE_DIRECTORY = Play.configuration.getProperty("fileDirectory");

    public static void contentDashboard(Integer newsTypeId) {
        Boolean headerContent = true;
        List<News> newsList;
        Account account = renderArgs.get("user", Account.class);
        List<NewsType> newsTypes = NewsType.findAll();
        Language language;
        if (play.i18n.Lang.get().equals("kz")) {
            language = Language.kz;
        } else {
            language = Language.ru;
        }
        if (newsTypeId != null) {
            NewsType newsType = NewsType.findById(newsTypeId);
            newsList = JPA.em().createQuery("Select n from News n where n.newsType = ?1 and n.deletedAt = null and n.language = ?2 and (n.group = ?3 or n.group is null) order by n.createdAt desc", News.class)
                    .setParameter(1, newsType)
                    .setParameter(2, language)
                    .setParameter(3, account.group)
                    .getResultList();
        } else {
            newsList = JPA.em().createQuery("Select n from News n where n.deletedAt = null and n.language = ?1 and (n.group = ?2 or n.group is null) order by n.createdAt desc", News.class)
                    .setParameter(1, language)
                    .setParameter(2, account.group)
                    .getResultList();
        }
        if (renderArgs.get("user", Account.class).group.key.equals("psychologist")) {
            renderArgs.put("psychologist", true);
        }
        if (renderArgs.get("user", Account.class).group.key.equals("doctor")) {
            renderArgs.put("doctor", true);
        }
        if (renderArgs.get("user", Account.class).group.key.equals("psychiatrist")) {
            renderArgs.put("psychiatrist", true);
        }
        render(headerContent, newsList, newsTypes);
    }

    public static void openNewsDocument(Integer newsId, String lang) {

        News news = News.findById(newsId);
        final File documentFile;
        if (lang != null && "kz".equals(lang)) {
            documentFile = new File(FILE_DIRECTORY, news.documentKz);
        } else {
            documentFile = new File(FILE_DIRECTORY, news.documentRu);
        }
        final String fileName = documentFile.getName();
        String contentType = URLConnection.guessContentTypeFromName(fileName);
        if (contentType == null || contentType.isEmpty()) {
            contentType = "text/plain";
        }
        response.setHeader("Content-Type", contentType);
        response.setHeader("Content-Disposition", "inline;filename=\"" + fileName + "\"");
        renderBinary(documentFile);
    }

    public static void contentView(Integer newsId) {
        Boolean headerContent = true;
        News news = News.findById(newsId);
        if (news.openDocumentOnView == true) {
            if (news.documentRu != null || news.documentKz != null) {
                final String lang = Lang.get();
                final File documentFile;
                if (news.documentRu == null || ("kz".equals(lang) && news.documentKz != null)) {
                    documentFile = new File(FILE_DIRECTORY, news.documentKz);
                }
                else {
                    documentFile = new File(FILE_DIRECTORY, news.documentRu);
                }
                final String fileName = documentFile.getName();
                String contentType = URLConnection.guessContentTypeFromName(fileName);
                if (contentType == null || contentType.isEmpty()) {
                    contentType = "text/plain";
                }
                response.setHeader("Content-Type", contentType);
                response.setHeader("Content-Disposition", "inline;filename=\"" + fileName + "\"");
                renderBinary(documentFile);
            }
        }

        render(news, headerContent);
    }

    public static void dashboard() {
        Account user = renderArgs.get("user", Account.class);
        if (!user.isFirstQuizFinished) {
            Step initTestStep = Step.find("group = ?1 and position = ?2", user.group, 0).first();
            if (initTestStep != null) {
                flash.put("headerContent", "true");
                Pages.quiz(initTestStep.id);
            }
        }
        List<Step> stepList = JPA.em().createQuery("Select s from Step s where s.group = ?1 and s.deletedAt = null order by position asc", Step.class)
                .setParameter(1, user.group).getResultList();
        List<Document> documents = JPA.em().createQuery("Select d from Document d where d.group = ?1 and d.step.position <= ?2 and d.language = ?3 order by d.step.position asc", Document.class)
                .setParameter(1, user.group)
                .setParameter(2, renderArgs.get("currentPosition", Integer.class))
                .setParameter(3, Language.valueOf((String) renderArgs.get("lang"))).getResultList();
        for (Step step: stepList) {
            List<Lesson> lessons = JPA.em().createQuery("Select l from Lesson l where l.step = ?1 and l.language = ?2 and deletedAt = null order by createdAt desc", Lesson.class)
                    .setParameter(1, step)
                    .setParameter(2, Language.valueOf((String) renderArgs.get("lang"))).getResultList();
            if (lessons != null && lessons.size() != 0) {
                step.actualLesson = lessons.get(0);
            }
        }
        Boolean headerContent = true;
        if (renderArgs.get("currentPosition", Integer.class) > user.group.lastQuestion()) {
            renderArgs.put("lastOpen", true);
        }
        if (user.isLastQuizFinished) {
            renderArgs.put("lastFinished", true);
            QuizScore quizScoreLast = QuizScore.find("Select q from QuizScore q where q.isPassed = true and q.step.position = 1000 and q.account = :user").setParameter("user", user).first();
            QuizScore quizScoreFirst = QuizScore.find("Select q from QuizScore q where q.isFinished = true and q.step.position = 0 and q.account = :user").setParameter("user", user).first();
            Integer percFirst = Math.round((float) quizScoreFirst.totalRight / (quizScoreFirst.totalWrong + quizScoreFirst.totalRight) * 100);
            Integer percLast = Math.round((float) quizScoreLast.totalRight / (quizScoreLast.totalWrong + quizScoreLast.totalRight) * 100);
            renderArgs.put("percFirst", percFirst);
            renderArgs.put("percLast", percLast);
        }
        render(stepList, documents, headerContent);
    }


    /**
     * This method renders lesson of the next step with respect to passed step
    @param stepId id of of the step
     */
    public static void lessonNext(Integer stepId) {
        Step step = Step.findById(stepId);
        Step nextStep = Step.find("group = ?1 and position = ?2", step.group, step.position + 1).first();
        if (nextStep == null || step.position == 0) {
            dashboard();
        }
        dashboard();
    }

    public static void lesson(Integer stepId) {
        Step step = Step.findById(stepId);
        if (step == null || step.position > renderArgs.get("currentPosition", Integer.class)) {
            dashboard();
        }
        Lesson lesson = Lesson.find("step = ?1 and language = ?2 and deletedAt = null", step, Language.valueOf((String) renderArgs.get("lang"))).first();
        if (lesson == null) {
            dashboard();
        }
        List<Document> documents = Document.find("group = ?1 and step = ?2 and language = ?3", renderArgs.get("user", Account.class).group, step, Language.valueOf((String) renderArgs.get("lang"))).fetch();
        render(lesson, step, documents);
    }

    public static void quizInfo(Integer stepId) {
        Step step = Step.findById(stepId);
        if (step == null || step.position > renderArgs.get("currentPosition", Integer.class)) {
            dashboard();
        }
        Quiz quiz = Quiz.find("step = ?1 and language = ?2", step, Language.valueOf((String) renderArgs.get("lang"))).first();
        if (quiz == null) {
            dashboard();
        }
        List<QuizScore> quizScores = QuizScore.find("quiz = ?1 and account = ?2 and quiz.step = ?3 and isFinished = true order by createdAt desc", quiz, renderArgs.get("user", Account.class), step).fetch();
        //checking if there at least on passed quiz
        boolean isPassed = false;
        for (QuizScore quizScore: quizScores) {
            if (quizScore.isPassed) {
                isPassed = true;
                break;
            }
        }
        render(quiz, stepId, quizScores, isPassed);
    }

    public static void quiz(Integer stepId) {
        //identify whether quiz has started already
        Account user = renderArgs.get("user", Account.class);
        Step step = Step.findById(stepId);
        if (step == null || step.position > renderArgs.get("currentPosition", Integer.class)) {
            if (step.position == 1000) {
                if (renderArgs.get("currentPosition", Integer.class) <= user.group.lastQuestion()) {
                    dashboard();
                }
            } else {
                dashboard();
            }
        }
        Quiz quiz = Quiz.find("step = ?1 and language = ?2", step, Language.valueOf((String) renderArgs.get("lang"))).first();
        if (quiz == null) {
            dashboard();
        }
        Long passedCount = QuizScore.count("quiz = ?1 and account = ?2 and isPassed = true", quiz, user);
        if (passedCount > 0) {
            dashboard();
        }
        if (flash.get("headerContent") != null && flash.get("headerContent").equals("true")) {
            renderArgs.put("headerContent", true);
        }
        QuizScore quizScore = QuizScore.find("quiz = ?1 and account = ?2 and isFinished = false", quiz, user).first();
        if (quizScore == null) {
            //if no quizScores, create one
            quizScore = new QuizScore(user, quiz);
            quizScore.save();
            renderQuestion(quiz, 0, quizScore);
        } else {
            //identify on which question user
            Integer maxQuestionPosition = JPA.em().createQuery("Select max(r.question.position) from QuizResult r where r.score = ?1", Integer.class)
                    .setParameter(1, quizScore)
                    .getSingleResult();
            if (maxQuestionPosition == null) {
                renderQuestion(quiz, 0, quizScore);
            }
            renderQuestion(quiz, maxQuestionPosition + 1, quizScore);
        }
    }

    private static void renderQuestion(Quiz quiz, Integer position, QuizScore quizScore) {
        List<QuizQuestion> questions = QuizQuestion.find("quiz = ?1 and deletedAt = null", quiz).fetch();
        Integer highestQuestionPosition = JPA.em().createQuery("Select max(q.position) from QuizQuestion q where q.quiz = ?1", Integer.class)
                .setParameter(1, quiz)
                .getSingleResult();
        if (position > highestQuestionPosition) {
            dashboard();
        } else {
            QuizQuestion currentQuestion = QuizQuestion.find("quiz = ?1 and position = ?2 and deletedAt = null", quiz, position).first();
            List<QuizAnswer> answers = QuizAnswer.find("quiz = ?1 and question = ?2 and deletedAt = null", quiz, currentQuestion).fetch();
            if (currentQuestion == null) {
                quizInfo(quiz.step.id);
            }
            renderTemplate("Pages/quizQuestion.html", quiz, questions, currentQuestion, answers, quizScore);
        }
    }

    public static void faq() {
        List<UserQuestion> userQuestions =
                JPA.em().createQuery("Select f from UserQuestion f where f.isActive = true and (f.isFaq = true or f.account = ?1)", UserQuestion.class).setParameter(1, renderArgs.get("user", Account.class)).getResultList();
        renderArgs.put("headerContent", true);
        render(userQuestions);
    }

    public static void saveQuestion(String question, boolean isAnonymous) {
        String currentLang = Lang.get();
        Account account = null;
        UserQuestion userQuestion;
        if (!isAnonymous) {
            account = renderArgs.get("user", Account.class);
        }

        if (currentLang.equals("kz")) {
            userQuestion = new UserQuestion(account,
                    null, null,
                    question, null,
                    null, null,
                    false);
        } else if (currentLang.equals("ru")) {
            userQuestion = new UserQuestion(account,
                    question, null,
                    null, null,
                    null, null,
                    false);
        } else {
            userQuestion = new UserQuestion(account,
                    null, null,
                    null, null,
                    question, null,
                    false);
        }
        userQuestion.save();
        renderText("Success");
    }

    public static void nextQuestion(Integer scoreId, Integer answerId) {
        Account user = renderArgs.get("user", Account.class);
        QuizScore quizScore = QuizScore.findById(scoreId);
        if (quizScore == null || quizScore.isFinished || quizScore.account.id != user.id) {
            dashboard();
        }
        Integer maxQuestionPosition = JPA.em().createQuery("Select max(r.question.position) from QuizResult r where r.score = ?1", Integer.class)
                .setParameter(1, quizScore)
                .getSingleResult();
        if (maxQuestionPosition == null) {
            maxQuestionPosition = 0;
        } else {
            maxQuestionPosition++;
        }
        QuizQuestion currentQuestion = QuizQuestion.find("quiz = ?1 and position = ?2 and deletedAt = null", quizScore.quiz, maxQuestionPosition).first();
        if (currentQuestion == null) {
            dashboard();
        }
        QuizAnswer answer = QuizAnswer.findById(answerId);
        if (answer == null || answer.question.id != currentQuestion.id) {
            dashboard();
        }
        QuizResult newQuizResult = new QuizResult();
        newQuizResult.isRight = answer.isRight;
        newQuizResult.answer = answer;
        newQuizResult.question = currentQuestion;
        newQuizResult.score = quizScore;
        newQuizResult.save();
        Integer highestQuestionPosition = JPA.em().createQuery("Select max(q.position) from QuizQuestion q where q.quiz = ?1", Integer.class)
                .setParameter(1, newQuizResult.score.quiz)
                .getSingleResult();
        if (currentQuestion.position == highestQuestionPosition) {
            quizScore.isFinished = true;
            quizScore.totalRight = (int) QuizResult.count("score = ?1 and isRight = true", quizScore);
            quizScore.totalWrong = (int) QuizResult.count("score = ?1 and isRight = false", quizScore);
            quizScore.isPassed = quizScore.totalRight >= quizScore.quiz.scoreToPass;
            quizScore.save();
            if (user.isFirstQuizFinished != null) {
                user.isFirstQuizFinished = true;
                user.save();
            }
            if (quizScore.isPassed && quizScore.quiz.step.position == 1000) {
                user.isLastQuizFinished = true;
                user.save();
            }
            renderTemplate("/Pages/quizScore.html", quizScore);
        }
        quiz(quizScore.quiz.step.id);
    }
}
