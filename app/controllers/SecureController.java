package controllers;

import models.Account;
import models.Profile;
import play.mvc.Before;
import play.mvc.Controller;

import javax.persistence.NoResultException;

/**
 * Created by sagynysh on 7/4/17.
 */
public class SecureController extends Controller {

    @Before(priority = 0)
    private static void checkSecurity() {
        if (!request.action.equals("SurveyController.survey") && !request.action.equals("SurveyController.surveyQuestions") && !request.action.equals("SurveyController.interruptSurvey") && !request.action.equals("SurveyController.submitResults")) {
            if (session.get("logged") == null) {
                Application.login();
            }
            renderArgs.put("username", session.get("logged"));
            if (play.i18n.Lang.get().equals("kz")) {
                renderArgs.put("lang", "kz");
            } else {
                renderArgs.put("lang", "ru");
            }
            Account user = Account.find("username = ?1", session.get("logged")).first();
            Profile profile = Profile.find("account = ?1", user).first();
            if (profile == null) {
                Settings.profile();
            }
            renderArgs.put("user", user);
            try {
                renderArgs.put("currentPosition", user.getCurrentPage() + 1);
            } catch (NoResultException e) {
                renderArgs.put("currentPosition", 1);
            }
        }
    }
}
