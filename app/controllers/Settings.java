package controllers;

import enums.Gender;
import models.Account;
import models.Profile;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.mvc.Before;
import play.mvc.Controller;
import utils.BCrypt;

/**
 * Created by tauyekel on 5/18/17.
 */
public class Settings extends Controller {

	@Before
	private static void checkSecurity() {
		if (session.get("logged") == null) {
			Application.login();
		}
		renderArgs.put("username", session.get("logged"));
	}

    public static void profile() {
        Boolean headerContent = true;
        Account user = Account.find("username = ?1", session.get("logged")).first();
        Profile profile = Profile.find("account = ?1", user).first();
        if (profile == null) {
            render(headerContent);
        }
    	String first_name = profile.firstName;
    	String last_name = profile.lastName;
    	Gender gender = profile.gender;
    	String phone = profile.phone;
    	String email = profile.email;
        render(first_name, last_name, gender, email, phone, headerContent);
    }

    public static void saveProfile(@Required String firstName,
                            @Required String lastName,
                            @Required Gender gender,
                            @Required String email,
                            @Required String phone) {
        if (Validation.hasErrors()) {
            profile();
        } else {
            Account user = Account.find("username = ?1", session.get("logged")).first();
            Profile profile = Profile.find("account = ?1", user).first();
            if (profile == null) {
                profile = new Profile();
                profile.account = user;
            }
            profile.firstName = firstName;
            profile.lastName = lastName;
            profile.gender = gender;
            profile.email = email;
            profile.phone = phone;
            profile.save();
            Pages.contentDashboard(null);
        }
    }

    public static void changePassword() {
        Boolean headerContent = true;
        renderArgs.put("error", flash.get("error"));
        render(headerContent);
    }

    public static void savePassword(String currentPassword, String newPassword, String newPasswordConfirm) {
        Account user = Account.find("username = ?1", session.get("logged")).first();
        if (!BCrypt.checkpw(currentPassword, user.password) || newPassword == null || !newPassword.equals(newPasswordConfirm) || newPassword.length() < 8) {
            flash.put("error", true);
            changePassword();
        } else {
            user.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
            user.save();
            Pages.dashboard();
        }
    }
}
