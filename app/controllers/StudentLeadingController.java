package controllers;

import com.itextpdf.text.DocumentException;
import dto.LeadingMessageDto;
import models.*;
import play.Logger;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Before;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.PdfCreator;
import utils.StudentLeadingUtils;

import javax.persistence.PersistenceException;
import java.io.IOException;
import java.util.*;

/**
 * Created by borikhankozha on 9/12/17.
 */
public class StudentLeadingController extends SecureController {

    @Before(priority = 1)
    public static void checkConditions() {
        Account account = renderArgs.get("user", Account.class);
        if (!account.group.key.equals("doctor") && !account.group.key.equals("psychiatrist")) {
            Pages.contentDashboard(null);
        }
        renderArgs.put("headerContent", true);
    }

    /**
     * Generates PDF file containing messages from doctor or psychiatrist roles.
     *
     * @param studentId the student id
     * @param groupKey the group key ("doctor" or "psychiatrist")
     * @param pages the string representing pages ex "1,2-4"
     * @return generated PDF file
     */
    public static void print(Integer studentId, String groupKey, String pages) {
        if (studentId != null) {
            Student student = Student.findById(studentId);
            if (student != null) {
                Set<Integer> pagesSet = StudentLeadingUtils.parsePagesString(pages);
                boolean isVop = groupKey.toLowerCase().contains("doctor");

                List<LeadingConversationMessage> conversationMessages = LeadingConversationMessage.find("student = ?1 order by createdAt asc", student).fetch();
                List<LeadingConversationMessage> messagesForPrinting = new ArrayList<>();
                int page = 1;
                for (LeadingConversationMessage message : conversationMessages) {
                    if (("doctor".equals(message.author.group.key) && isVop)
                            || (!isVop && ("psychiatrist".equals(message.author.group.key)))) {
                        if (pagesSet.contains(page)) {
                            messagesForPrinting.add(message);
                        }
                        page++;
                    }
                }
                String position = "";
                if (isVop) {
                    position = Messages.get("VOP");
                } else {
                    position = Messages.get("psycho");
                }
                Map<String, Object> params = new HashMap<>(3);
                params.put("messagesForPrinting", messagesForPrinting);
                params.put("position", position);

                String path = "StudentLeadingController/studentLeadingPdf.html";
                Template template = TemplateLoader.load(path);
                String pageContents = template.render(params);

                response.setHeader("Content-Type", "application/pdf");
                response.setHeader("Content-Disposition", "inline;filename=\"" + student.code + ".pdf\"");
                try {
                    renderBinary(PdfCreator.fromStudentLeadingHtml(student.code, pageContents));
                } catch (IOException e) {
                    Logger.error(e, "IO Exception during creating student leading document");
                } catch (DocumentException e) {
                    Logger.error(e, "IO Exception during creating student leading document");
                }
            }
            else {
                Logger.debug("Student with id " + studentId + " was not found");
                notFound();
            }
        }
        else {
            Logger.debug("StudentId is empty");
            notFound();
        }
    }

    public static void index(String code) {
        renderArgs.put("newStudent", flash.get("newStudent"));
        Account user = renderArgs.get("user", Account.class);
        List<Student> students;
        if (code == null || code.trim().equals("")) {
            students = JPA.em().createNativeQuery("Select * from students s, students_and_accounts sa " +
                    "where s.id = sa.student_id and sa.account_id = ?1 and s.deleted_at is null", Student.class).setParameter(1, user.id).getResultList();
        } else {
            renderArgs.put("code", code);
            code = "%" + code.trim().replaceAll("-", "") + "%";
            students = JPA.em().createNativeQuery("Select * from students s, students_and_accounts sa " +
                    "where s.id = sa.student_id and sa.account_id = ?1  and s.deleted_at is null and s.code like ?2", Student.class).setParameter(1, user.id).setParameter(2, code).getResultList();
        }
        Long allCount = StudentAndAccount.count("account_id = ?1", user.id);
        render(students, allCount);
    }

    public static void newStudent() {
        renderArgs.put("back", true);
        renderArgs.put("backUrl", "/leading/index");
        renderArgs.put("error", flash.get("error"));
        render();
    }

    public static void saveStudent(String number) {
        if (number == null) {
            flash.put("error", "error");
            newStudent();
        }
        number = number.trim().replace("-", "");
        Account account = renderArgs.get("user", Account.class);
        Student student = Student.find("code = ?1", number).first();
        if (student == null || student.status.equals(Student.Status.created)) {
            flash.put("error", "error");
            newStudent();
        }
        InterviewResult interviewResult = InterviewResult.find("student = ?1", student).first();
        SurveyResult surveyResult = SurveyResult.find("student = ?1", student).first();
        if (!student.status.equals(Student.Status.interrupted)) {
            if (interviewResult == null || surveyResult == null) {
                flash.put("error", "error");
                newStudent();
            }
            if (!(surveyResult.risk.equals(SurveyResult.Risk.high) || surveyResult.risk.equals(SurveyResult.Risk.presents)) || interviewResult.verdict != InterviewResult.Verdict.approved) {
                flash.put("error", "error");
                newStudent();
            }
        }
        if (account.group.key.equals("psychiatrist")) {
            StudentAndAccount existing = StudentAndAccount.find("student = ?1 and account.group.key = ?2", student, "psychiatrist").first();
            if (existing != null) {
                if (!existing.account.id.equals(account.id)) {
                    flash.put("error", existing.account.username);
                    newStudent();
                }
            }
        }
        StudentAndAccount studentAndAccount = new StudentAndAccount();
        studentAndAccount.student = student;
        studentAndAccount.account = account;
        try {
            studentAndAccount.save();
            flash.put("newStudent", student.dashedCode());
            index(null);
        } catch (PersistenceException e) {
            flash.put("error", "exists");
            newStudent();
        }
    }

    public static void deleteStudent(Integer id) {
        Account account = renderArgs.get("user", Account.class);
        StudentAndAccount studentAndAccount = null;
        if (id != null) {
            Student student = Student.findById(id);
            if (student != null) {
                studentAndAccount = StudentAndAccount.find("student = ?1 and account = ?2", student, account).first();
            }
        }
        if (studentAndAccount != null) {
            studentAndAccount.delete();
        }
        index(null);
    }

    public static void chat(String code) {
        Account account = renderArgs.get("user", Account.class);
        renderArgs.put("back", true);
        renderArgs.put("backUrl", "/leading/index");
        Student student = Student.findByCode(code);
        if (student == null || student.status.equals(Student.Status.created)) {
            Pages.contentDashboard(null);
        }
        InterviewResult interviewResult = InterviewResult.find("student = ?1", student).first();
        SurveyResult surveyResult = SurveyResult.find("student = ?1", student).first();
        if (!student.status.equals(Student.Status.interrupted)) {
            if (interviewResult == null || surveyResult == null) {
                Pages.contentDashboard(null);
            }
            if (!(surveyResult.risk.equals(SurveyResult.Risk.high) || surveyResult.risk.equals(SurveyResult.Risk.presents)) || interviewResult.verdict != InterviewResult.Verdict.approved) {
                Pages.contentDashboard(null);
            }
        }
        List<LeadingMessageDto> messageDtoList = new ArrayList<LeadingMessageDto>();
        List<LeadingConversationMessage> conversationMessages;
        conversationMessages = LeadingConversationMessage.find("student = ?1", student).fetch();
        int order = 0;
        for (LeadingConversationMessage conversationMessage : conversationMessages) {
            if (conversationMessage.parent == null) {
                LeadingMessageDto messageDto = new LeadingMessageDto();
                messageDto.vopMessage = conversationMessage;
                for (LeadingConversationMessage conversationMessageInside : conversationMessages) {
                    if (conversationMessageInside.parent != null
                            && conversationMessage.id == conversationMessageInside.parent.id) {
                        messageDto.psychoMessage = conversationMessageInside;
                        break;
                    }
                }
                messageDto.order = ++order;
                messageDtoList.add(messageDto);
            }
        }
        int total = order;
        code = student.dashedCode();
        render(code, messageDtoList, total);
    }

    public static void saveMessage(String code, Integer parentId, String message) {
        if (message == null || message.trim().equals("")) {
            chat(code);
        }
        Account account = renderArgs.get("user", Account.class);
        int type = 0;
        if (account.group.key.equals("doctor")) {
            type = 1;
        }
        if (account.group.key.equals("psychiatrist")) {
            type = 2;
        }
        if (type == 0) {
            Pages.contentDashboard(null);
        }
        Student student = Student.findByCode(code);
        if (student == null) {
            Pages.contentDashboard(null);
        }
        StudentAndAccount studentAndAccount = StudentAndAccount.find("student = ?1 and account = ?2", student, account).first();
        if (studentAndAccount == null) {
            Pages.contentDashboard(null);
        }
        LeadingConversationMessage conversationMessage = new LeadingConversationMessage();
        Profile profile = Profile.find("account = ?1", account).first();
        conversationMessage.student = student;
        conversationMessage.author = account;
        conversationMessage.message = message;
        conversationMessage.authorProfile = profile;
        if (type == 1) {
            conversationMessage.parent = null;
        } else {
            if (parentId == null) {
                Pages.contentDashboard(null);
            }
            LeadingConversationMessage parent = LeadingConversationMessage.findById(parentId);
            if (parent == null) {
                Pages.contentDashboard(null);
            }
            LeadingConversationMessage existingChild = LeadingConversationMessage.find("parent = ?1", parent).first();
            if (existingChild != null) {
                chat(code);
            }
            conversationMessage.parent = parent;
        }
        conversationMessage.save();
        chat(student.code);
    }
}
