package dto;

import models.LeadingConversationMessage;

/**
 * Created by borikhankozha on 9/14/17.
 */
public class LeadingMessageDto {

    public LeadingMessageDto() {
    }

    public int order;
    public LeadingConversationMessage vopMessage;
    public LeadingConversationMessage psychoMessage;

}
