package dto;

public class OfflineStudentsArrayDto {

    public class OfflineStudentDto {
        public String status;
        public String code;
        public String gender;
        public String createdDate;
        public String updatedDate;
        public String deletedDate;
        public OfflineInterruptedDto interrupted;
        public OfflineSurveyAnswersDto surveyResult;
        public OfflineInterviewDto interviewResult;
    }

    public class OfflineSurveyAnswersDto extends SurveyAnswersDto {
        public String date;
    }

    public class OfflineInterruptedDto {
        public Integer question;
        public String state;
    }

    public class OfflineInterviewDto {
        public String verdict;
        public String text;
        public String updatedDate;
    }

    public OfflineStudentDto[] students;
}
