package dto;

import java.util.List;

public class RiskDataDto {

    public Integer id;
    public String name;
    public Long maleCount;
    public Long femaleCount;
    public Long sum;
    public List<RiskDataDto> children;
    public String type;
}
