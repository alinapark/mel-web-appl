package dto;

import java.util.List;

/**
 * Created by sagynysh on 7/20/17.
 */
public class SurveyAnswersDto {

    public class QuestionElevenDto {
        public Integer[] array;
        public String firstText;
        public String secondText;
    }

    public String code;
    public String login;
    public Integer q1;
    public Integer q2;
    public Integer q3;
    public List<Integer> q4;
    public List<Integer> q5;
    public Integer q6;
    public Integer q7;
    public Integer q8;
    public String q9;
    public Integer q10;
    public QuestionElevenDto q11;
    public List<Integer> q12;
    public List<Integer> q13;

    public Boolean hasNotTalkedToSomeone() {
        boolean noone = false;
        if (q11 == null || q11.array == null) {
            return null;
        }
        for (Integer i: q11.array) {
            if (i == 5) {
                noone = true;
            }
        }
        return noone;
    }
}
