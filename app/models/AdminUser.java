package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 5/31/17.
 */
@Entity
@Table(name="admins")
public class AdminUser extends GenericModel {

    public AdminUser(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="email")
    public String email;

    @Column(name="password")
    public String password;

}
