package models;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.List;

/**
 * Created by sagynysh on 5/17/17.
 */
@Entity
@Table(name="groups")
public class Group extends GenericModel {

    public Group(String name, String key) {
        this.name = name;
        this.key = key;
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="key")
    public String key;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    public List<Account> accounts;

    @Transient
    public Integer lastQuestion() {
        return (Integer) JPA.em().createNativeQuery("Select max(position) from steps where position != 1000 and group_id = ?1 and deleted_at is null").setParameter(1, this.id).getSingleResult();
    }
}
