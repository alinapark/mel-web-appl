package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sagynysh on 8/6/17.
 */
@Entity
@Table(name="interview_results")
public class InterviewResult extends GenericModel {

    public enum Verdict {
        approved, refuted
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name="risk", columnDefinition = "ENUM('high', 'presents', 'low', 'no')")
    public SurveyResult.Risk risk;

    @Enumerated(EnumType.STRING)
    @Column(name="verdict", columnDefinition = "ENUM('approved', 'refuted')")
    public Verdict verdict;

    @Column(name="text_", columnDefinition = "TEXT")
    public String text;

    @ManyToOne
    @JoinColumn(name="student_id")
    public Student student;

    @Column(name="updated_at")
    public Date updatedAt;

    @Transient
    public String formattedDate() {
        if (updatedAt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(updatedAt);
    }
}
