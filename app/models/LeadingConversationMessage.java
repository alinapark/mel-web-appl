package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by borikhankozha on 9/14/17.
 */
@Entity
@Table(name="leading_conversation_messages")
public class LeadingConversationMessage extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name="student_id")
    public Student student;

    @ManyToOne
    @JoinColumn(name="author_id")
    public Account author;

    @OneToOne
    @JoinColumn(name="author_profile_id")
    public Profile authorProfile;

    @Column(name="message", columnDefinition = "TEXT")
    public String message;

    @Column(name="created_at")
    public Date createdAt;

    @ManyToOne
    @JoinColumn(name="parent_message_id")
    public LeadingConversationMessage parent;

    @Transient
    public String createdAt() {
        if (createdAt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return format.format(createdAt);
    }
}
