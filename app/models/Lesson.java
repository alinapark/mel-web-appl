package models;

import enums.Language;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by sagynysh on 5/18/17.
 */
@Entity
@Table(name="lessons")
public class Lesson extends GenericModel {

    public Lesson(String name, String description, String content, String video, String picture, String subtitles, Language language, Group group, Step step) {
        this.name = name;
        this.description = description;
        this.content = content;
        this.video = video;
        this.picture = picture;
        this.subtitles = subtitles;
        this.language = language;
        this.group = group;
        this.step = step;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="description", columnDefinition = "TEXT")
    public String description;

    @Column(name="content", columnDefinition = "LONGTEXT")
    public String content;

    @Column(name="video")
    public String video;

    @Column(name="picture")
    public String picture;

    @Column(name="subtitles")
    public String subtitles;

    @Enumerated(EnumType.STRING)
    @Column(name="language", columnDefinition = "ENUM('ru', 'kz')")
    public Language language;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @ManyToOne
    @JoinColumn(name="step_id")
    public Step step;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;
}
