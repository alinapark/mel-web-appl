package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 6/18/17.
 */
@Entity
@Table(name="locations")
public class Location extends GenericModel {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @ManyToOne
    @JoinColumn(name="parent_id_0")
    public Location parent0;

    @ManyToOne
    @JoinColumn(name="parent_id_1")
    public Location parent1;

    @ManyToOne
    @JoinColumn(name="parent_id_2")
    public Location parent2;

    @Column(name="position")
    public Integer position;
}