package models;

import enums.Language;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tauyekel on 6/19/17.
 */

@Entity
@Table(name="news")
public class News extends GenericModel {

    public News(String name, String content, String description, NewsType newsType, String video, String picture, Language language, Group group) {
        this.name = name;
        this.content = content;
        this.video = video;
        this.picture = picture;
        this.language = language;
        this.group = group;
        this.newsType = newsType;
        this.description = description;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="description", columnDefinition = "TEXT")
    public String description;

    @Column(name="content", columnDefinition = "LONGTEXT")
    public String content;

    @Column(name="video")
    public String video;

    @Column(name="picture")
    public String picture;

    /*
    * Some news can contain resources booklets, presentation and etc.
    * This field for document in Russian.
    * */
    @Column(name="document_ru")
    public String documentRu;

    /*
    * Some news can contain resources booklets, presentation and etc.
    * This field for document in Kazakh.
    * */
    @Column(name="document_kz")
    public String documentKz;

    @Enumerated(EnumType.STRING)
    @Column(name="language", columnDefinition = "ENUM('ru', 'kz')")
    public Language language;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @ManyToOne
    @JoinColumn(name="news_type_id")
    public NewsType newsType;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;

    @Column(name="open_document_on_view", columnDefinition = "bit", nullable = false)
    public Boolean openDocumentOnView;
}
