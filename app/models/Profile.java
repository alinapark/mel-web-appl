package models;

import enums.Gender;
import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 5/18/17.
 */
@Entity
@Table(name="profiles")
public class Profile extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="first_name")
    public String firstName;

    @Column(name="last_name")
    public String lastName;

    @Column(name="email")
    public String email;

    @Column(name="phone")
    public String phone;

    @Enumerated(EnumType.STRING)
    @Column(name="gender", columnDefinition = "ENUM('m', 'f')")
    public Gender gender;

    @ManyToOne
    @JoinColumn(name="account_id")
    public Account account;
}
