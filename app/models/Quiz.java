package models;

import enums.Language;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by sagynysh on 5/19/17.
 */
@Entity
@Table(name="quizzes")
public class Quiz extends GenericModel {

    public Quiz(String name, String description, String content, Language language, Integer scoreToPass, Group group, Step step) {
        this.name = name;
        this.description = description;
        this.content = content;
        this.language = language;
        this.scoreToPass = scoreToPass;
        this.group = group;
        this.step = step;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="description", columnDefinition = "TEXT")
    public String description;

    @Column(name="content", columnDefinition = "TEXT")
    public String content;

    @Enumerated(EnumType.STRING)
    @Column(name="language", columnDefinition = "ENUM('ru', 'kz')")
    public Language language;

    @Column(name="score_to_pass")
    public Integer scoreToPass;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @ManyToOne
    @JoinColumn(name="step_id")
    public Step step;

    @OneToMany(mappedBy = "quiz", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @OrderBy("position")
    public List<QuizQuestion> quizQuestions;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;
}
