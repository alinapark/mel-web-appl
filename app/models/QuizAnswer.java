package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by tauyekel on 5/24/17.
 */
@Entity
@Table(name="quiz_answers")
public class QuizAnswer extends GenericModel {

    public QuizAnswer(Quiz quiz, QuizQuestion question, String value, Boolean isRight) {
        if (value != null) {
            value = value.trim();
        }
        this.quiz = quiz;
        this.question = question;
        this.value = value;
        this.isRight = isRight;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name="quiz_id")
    public Quiz quiz;

    @ManyToOne
    @JoinColumn(name="question_id")
    public QuizQuestion question;

    @Column(name="value")
    public String value;

    @Column(name="is_right" ,columnDefinition = "BIT", length = 1)
    public Boolean isRight;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;
}
