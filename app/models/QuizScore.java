package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by sagynysh on 5/19/17.
 */
@Entity
@Table(name="quiz_scores")
public class QuizScore extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name="quiz_id")
    public Quiz quiz;

    @ManyToOne
    @JoinColumn(name="account_id")
    public Account account;

    @Column(name="total_right")
    public Integer totalRight;

    @ManyToOne
    @JoinColumn(name="step_id")
    public Step step;

    @Column(name="total_wrong")
    public Integer totalWrong;

    @Column(name="is_finish", columnDefinition = "bit", nullable = false)
    public Boolean isFinished;
    
    @Column(name="is_passed", columnDefinition = "bit", nullable = false)
    public Boolean isPassed;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @OneToMany(mappedBy = "answer", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public List<QuizResult> quizResultList;

    public QuizScore(Account account, Quiz quiz) {
        this.totalRight = 0;
        this.totalWrong = 0;
        this.isFinished = false;
        this.isPassed = false;
        this.createdAt = new Date();
        this.updatedAt = new Date();
        this.account = account;
        this.quiz = quiz;
        this.step = quiz.step;
    }

    public String getFormattedTime() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        return timeFormat.format(updatedAt);
    }

    public String getFormattedDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(updatedAt);
    }
}
