package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by sagynysh on 5/18/17.
 */
@Entity
@Table(name="steps")
public class Step extends GenericModel {

    public Step(String name, Integer position, String color, Group group, Boolean isModerForward) {
        this.name = name;
        this.color = color;
        this.position = position;
        this.group = group;
        this.isModerForward = isModerForward;
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="position")
    public Integer position;

    @Column(name="color")
    public String color;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @Column(name="is_moder_forward", columnDefinition = "BIT", length = 1)
    public Boolean isModerForward;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;

    @OneToMany(mappedBy = "step", fetch = FetchType.EAGER)
    public List<Lesson> lessons;

    @Transient
    public Lesson actualLesson;

    @Transient
    public String style(String type) {
        if (!this.color.equals("transparent") && !this.color.equals("#000000")) {
            return type.equals("title") ? " style=\"color: #ffffff;\"" : "style=\"background-color: " + this.color + ";\"";
        } else {
            return "";
        }
    }
}
