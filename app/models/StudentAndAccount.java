package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by borikhankozha on 9/12/17.
 */
@Entity
@Table(name="students_and_accounts")
public class StudentAndAccount extends GenericModel{

    @Id
    @ManyToOne
    @JoinColumn(name="student_id")
    public Student student;

    @Id
    @ManyToOne
    @JoinColumn(name="account_id")
    public Account account;
}
