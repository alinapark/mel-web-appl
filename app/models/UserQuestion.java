package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by borikhankozha on 8/28/17.
 */
@Entity
@Table(name="faq_questions")
public class UserQuestion extends GenericModel {

    public UserQuestion() {

    }

    public UserQuestion(Account account,
                        String questionRu,
                        String answerRu,
                        String questionKz,
                        String answerKz,
                        String questionEn,
                        String answerEn,
                        boolean isActive) {
        this.account = account;
        this.questionRu = questionRu;
        this.answerRu = answerRu;
        this.questionEn = questionEn;
        this.answerEn = answerEn;
        this.questionKz = questionKz;
        this.answerKz = answerKz;
        this.isActive = isActive;
        this.createdAt = new Date();
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name = "who_asked")
    public Account account;

    @Column(name = "question_ru", columnDefinition = "TEXT")
    public String questionRu;

    @Column(name = "question_en", columnDefinition = "TEXT")
    public String questionEn;

    @Column(name = "question_kz", columnDefinition = "TEXT")
    public String questionKz;

    @Column(name = "answer_ru", columnDefinition = "TEXT")
    public String answerRu;

    @Column(name = "answer_kz", columnDefinition = "TEXT")
    public String answerKz;

    @Column(name = "answer_en", columnDefinition = "TEXT")
    public String answerEn;

    @Column(name = "created_at")
    public Date createdAt;

    @Column(name = "is_active", columnDefinition = "BIT", length = 1)
    public boolean isActive;

    @Column(name = "is_faq", columnDefinition = "BIT", length = 1)
    public boolean isFaq;

    @Transient
    public boolean isRussianReady() {
        if (answerRu == null || answerRu.trim().equals("")) {
            return false;
        }
        if (questionRu == null || questionRu.trim().equals("")) {
            return false;
        }
        return true;
    }

    @Transient
    public boolean isEnglishReady() {
        if (answerEn == null || answerEn.trim().equals("")) {
            return false;
        }
        if (questionEn == null || questionEn.trim().equals("")) {
            return false;
        }
        return true;
    }

    @Transient
    public boolean isKazakhReady() {
        if (answerKz == null || answerKz.trim().equals("")) {
            return false;
        }
        if (questionKz == null || questionKz.trim().equals("")) {
            return false;
        }
        return true;
    }

    @Transient
    public String createdAt() {
        if (createdAt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return format.format(createdAt);
    }
}
