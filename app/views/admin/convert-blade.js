var fs = require('fs');
var glob = require('glob');
var path = require('path');

// List all files in a directory in Node.js recursively in a synchronous fashion
var walkSync = function(dir, filelist) {
  var fs = fs || require('fs'),
      files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = walkSync(dir + '/' + file, filelist);
    }
    else {
      filelist.push(dir + '/' + file);
    }
  });
  return filelist;
};

let files = [];

walkSync('.', files);
// console.log(files);


files.map((fileName) => {
	console.log(fileName + "\n");
	// if (fileName.includes("blade")) {
	// 	let newFileName = fileName.replace(/blade.php/g, 'html');
	// 	fs.renameSync(fileName, newFileName);
	// }
})

//get file names
// bladeFiles = fs.readdirSync('/').filter(file => {
// 	return file.includes("blade");
// });

// console.log(bladeFiles);

//rename them
// fs.renameSync('public/css/' + cssFileName, 'public/css/app.css');
// fs.renameSync('public/js/' + jsFileName, 'public/js/app.js');