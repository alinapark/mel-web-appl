window.autosize = require('autosize');
global.$ = global.jQuery = require('jquery');

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

require('./functions');
