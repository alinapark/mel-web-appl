$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

let fieldable = {
    regions: {
        block: $('[data-location=regions]'),
        field: $('[data-location=regions] select')
    },
    provinces: {
        block: $('[data-location=provinces]'),
        field: $('[data-location=provinces] select')
    },
    cities: {
        block: $('[data-location=cities]'),
        field: $('[data-location=cities] select')
    },
    schools: {
        block: $('[data-location=schools]'),
        field: $('[data-location=schools] select'),
    },
    findSelect: function (changed, elem) {
        var data = elem.attr('data-location');

        return changed.parents('.__js-location').find('[data-location=' + data + '] select');
    },
    loadSchools: function (changed, params) {
        if (!changed.parents('.__js-location').has('[data-location=schools]').length) {
            return;
        }

        var select = this.findSelect(changed, this.schools.block);

        var data = {
            region_id: this.findSelect(changed, this.regions.block).val(),
            province_id: this.findSelect(changed, this.provinces.block).val(),
            city_id: this.findSelect(changed, this.cities.block).val()
        };

        $.ajax({
            method: 'GET',
            url: `/ajax/schools?region_id=${data.region_id}&province_id=${data.province_id}&city_id=${data.city_id}`
        }).done(function (response) {
            var options = fieldable.schools.field;

            $.each(response, function() {
                options.append($("<option />").val(this.id).text(this.name));
            });
        });
    },
    loadLocations: function (changed, elem, key) {
        var data = {};
        var select = this.findSelect(changed, elem);

        data[key] = changed.val();

        $.ajax({
            method: 'GET',
            url: `/ajax/${elem.attr('data-location')}/${data[key]}`
        }).done(function (response) {
            var options = fieldable[elem.attr('data-location')].field;

            $.each(response, function() {
                options.append($("<option />").val(this.id).text(this.name));
            });
        });
    },
    clear: function (changed, elem) {
        var select = this.findSelect(changed, elem);

        select.html('<option></option>');
    }
};

// Change of Region field
fieldable.regions.field.on('change', function () {
    // Clear Province, City and School fields
    fieldable.clear($(this), fieldable.provinces.block);
    fieldable.clear($(this), fieldable.cities.block);
    fieldable.clear($(this), fieldable.schools.block);

    // Load data for Province field
    fieldable.loadLocations($(this), fieldable.provinces.block, 'region_id');

    fieldable.loadSchools($(this));
});

// Change of Province field
fieldable.provinces.field.on('change', function () {
    // Clear City and School fields
    fieldable.clear($(this), fieldable.cities.block);
    fieldable.clear($(this), fieldable.schools.block);

    // Load data for City field
    fieldable.loadLocations($(this), fieldable.cities.block, 'province_id');

    fieldable.loadSchools($(this));
});

// Change of City field
fieldable.cities.field.on('change', function () {
    // Clear School field
    fieldable.clear($(this), fieldable.schools.block);

    // Load data for School field
    fieldable.loadSchools($(this));
});