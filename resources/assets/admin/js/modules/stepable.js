$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

let stepable = {
    group: {
        block: $('#field-group'),
        field: $('#field-group select')
    },
    step: {
        block: $('#field-step'),
        field: $('#field-step select')
    }
};



stepable.group.field.on('change', function () {
    stepable.step.field[0].options.length = 0

    if (! $(this).val() ) {
        stepable.step.block.addClass('is-hidden');
        stepable.step.field.html('');
    } else {
        $.ajax({
            method: 'GET',
            url: `/ajax/steps/${stepable.group.field.val()}`
        }).done(function (response) {
            var options = stepable.step.field;

            $.each(response, function() {
                options.append($("<option />").val(this.id).text(this.name));
            });
        });

        stepable.step.block.removeClass('is-hidden');
    }
});
