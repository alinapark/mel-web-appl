global.$ = global.jQuery = require('jquery');

function open_submenu() {
    if ($('[data-submenu-open]').length > 0) {
        var doc_click_close = function(e) {
            if ($(e.target).closest('[data-submenu]').length == 0 && $(e.target).closest('[data-submenu-open]').length == 0) {
                $('[data-submenu-open]').removeClass('is-active');
                $('[data-submenu]').slideUp(300);
            }
        };
        $('[data-submenu-open]').on('click', function() {
            if (!$(this).hasClass('is-active')) {
                // $('[data-submenu-open]').removeClass('is-active');
                // $('[data-submenu]').slideUp(200);
                $(this).addClass('is-active');
                $('[data-submenu="' + $(this).data('submenu-open') + '"]').slideDown(200);
                $(document).off('click', doc_click_close);
                $(document).on('click', doc_click_close);
            } else {
                $(this).removeClass('is-active');
                $('[data-submenu="' + $(this).data('submenu-open') + '"]').slideUp(200);
                $(document).off('click', doc_click_close);
            }
        });
    } else {
        return false;
    }
}

function block_scroll(bool) {
    if (bool) {
        $('body').addClass('block-scroll');
    } else {
        $('body').removeClass('block-scroll');
    }
}

$(function() {

    open_submenu();

    if ($(window).width() < 768) {
        $('.header__device-menu').on('click', function() {
            if ($(this).hasClass('is-active')) {
                block_scroll(true);
            } else {
                block_scroll(false);
            }
        });
    }

});

if ($('.course-block__body-switcher').length > 0) {
    $('.course-block').on('click', '.course-block__body-switcher', function() {
        if (!$(this).closest('.course-block').hasClass('is-active')) {
            $(this).closest('.course-block').addClass('is-active');
            $(this).closest('.course-block').children('.course-block__body').slideDown(200);
        } else {
            $(this).closest('.course-block').removeClass('is-active');
            $(this).closest('.course-block').children('.course-block__body').slideUp(200);
        }
    });
}
