var acc = document.getElementsByClassName("entity");
var i;
var state=true;

for (i = 0; i < acc.length; i++)
{
    acc[i].onclick = function()
    {

        this.classList.toggle("active");

        header:"div.entity_question"

        var ent_image = this.getElementsByClassName("q-image");
        var entity_answer = this.nextElementSibling;

        if (entity_answer.style.maxHeight)
        {
            entity_answer.style.maxHeight = null;
            $(ent_image[0]).css("transform", "");
        }
        else
        {
            entity_answer.style.maxHeight = entity_answer.scrollHeight + "px";
            $(ent_image[0]).css("transform", "rotate(90deg)");
        }
    }
}

$(document).ready(function() {

    $("#ask_div").click(function() {
        $(".modal-overlay").show();
        $("#ask-modal").css('display', 'flex');
    });

    $("#ask-modal-close").click(function() {
        $(".modal-overlay").hide();
        $("#ask-modal").hide();
     });

    $('#send-button').click(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
        $("#modal-success").hide();
        $("#modal-failure").hide();

        var question = $("#modal-textarea").val();
        if (question.trim().length == 0 ) {
            return;
        }
        var anon_val = $("#anonymous").is(':checked') ? true : false;
        $.ajax({
            url: "/pages/savequestion",
            type: "POST",
            data: "question=" + encodeURIComponent(question.trim()) + "&isAnonymous=" + anon_val,
            contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            dataType: "text"
        }).done(function(data) {
            $("#modal-success").show();
        }).fail(function(data) {
            $("#modal-failure").show();
        });
    });
});