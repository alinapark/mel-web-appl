DROP TABLE IF EXISTS faq_questions;
CREATE TABLE faq_questions (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    who_asked int(10) UNSIGNED,
    question_ru text,
    question_kz text,
    question_en text,
    answer_ru text,
    answer_kz text,
    answer_en text,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    is_active bit(1) not null default false,
    is_faq bit(1) not null default false,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

insert into faq_questions(question_ru, answer_ru, question_kz, answer_kz, question_en, answer_en, is_active, is_faq)
values("Какая миссия проекта Жастар?",
		"Проект способствует формированию позитивного отношения к здоровому образу жизни, а также и развитию социальных навыков молодых людей, обучающихся в школах и колледжах. Для достижения этой цели проект предлагает мультимедийные материалы для учащихся, дает актуальные рекомендации родителям, обучает психологов, врачей общей практики и врачей психиатрического профиля; сотрудничает с работниками СМИ и правоохранительных органов",
        "Жастар проектiнiң миссиясы қандай?",
        "Жоба мектептер мен колледждерде оқитын жастарда салауатты өмір салтын қалыптастыру мен әлеуметтік дағдыларын дамытуға мүмкіндік береді. Бұл мақсатқа жетуде жоба оқушыларға арналған мультимедиалық материалдар ұсынады және ата-аналарға маңызды ұсынымдар мен кеңестер береді; психолог, жалпы тәжірибе дәрігерлері мен психиатрия саласының дәрігерлерін оқытады; БАҚ және құқық қорғау органдарының өкілдерімен бірігіп қызмет жасайды.",
        "What is a goal of Zhastar project?",
        "The project helps to form positive attitude towards healthy life and to develop social life-skills of young people studying in colleges and high school. In order to reach the stated goal the project proposes multimedia material for students, gives relevant recommendations for parents, teaches psychologists, doctors and psychiatrists; the project also cooperates with mass media and law enforcement agencies.",
        true,
        true
        );

insert into faq_questions(question_ru, answer_ru, question_kz, answer_kz, question_en, answer_en, is_active, is_faq)
values("Для кого создан проект?",
		"Проект создан для созидания условий счастливого детства и юношества. Для этого проект обучает родителей, психологов, психиатров, врачей общей практики и самих детей и подростков.",
        "Проект кімге арналған?",
        "Проект балалар мен жасөспірімдердің бақытты өмірсүруіне жағдай жасауға арналады. Осы мақсатпен проект ата-аналарды, психологтарды, психиатрлар мен жалпы тәжірибе дәрігерлерін оқытады.",
        "For whom the project is created?",
        "The project is dedicated to creation of opportune conditions for happy youth and childhood. For this reason we teach parents, psychologists, psychotherapists and doctors",
        true,
        true
        );

insert into faq_questions(question_ru, answer_ru, question_kz, answer_kz, question_en, answer_en, is_active, is_faq)
values("Что предлагает проект учителям?",
		"Учителя могут ознакомиться и изучить обучающие материалы по выявлению физических и эмоциональных изменений у учащихся и скорейшему реагированию в целях недопущения негативных последствий.",
        "Проект оқытушыларға не ұсынады?",
        "Ұстаздар білім алушылардағы эмоциялық өзгерістерді дер кезінде байқап, жағымсыз салдардың алдын алуға мүмкіндік беретін материалдармен таныса алады.",
        "What does the project propose to teachers?",
        "Teachers can get familiar with and study new materials about detection of physical and emotional changes in students and tutorials about appropriate reaction in order to prevent negative consequences.",
        true,
        true
        );

insert into faq_questions(question_ru, answer_ru, question_kz, answer_kz, question_en, answer_en, is_active, is_faq)
values("Что предлагает проект родителям?",
		"Для родителей проект предоставляет рекомендации по повышению эффективности взаимодействия с детьми, а также позволяет приобрести новые знания для развития у них жизненных навыков.",
        "Проект ата-аналарға не ұсынады?",
        "Бұл жоба ата-аналарға балаларымен жақсы қарым-қатынас орнату және балаларының бойында өмірлік маңызы бар дағдыларды дамыту үшін жаңа білім алуға көмектеседі.",
        "What does the project propose to parents?",
        "Parents can find recommendations about increasing efficiency of interaction with their children. They can gain new knowledge about developing life-skills in their children.",
        true,
        true
        );

insert into faq_questions(question_ru, answer_ru, question_kz, answer_kz, question_en, answer_en, is_active, is_faq)
values("Что предлагает проект учащимся?",
		"Для учащихся на портале выложены мультимедийные материалы, которые позволяют приобрести навыки общения, поддержания здорового образа жизни и самосохранения в критических ситуациях. Указанные материалы также могут служить помощником в выборе профессии и вопросах трудоустройства.",
        "Проект оқушыларға не ұсынады?",
        "Порталда білім алушылар үшін мультимедиялық материалдар берілген. Бұл мультимедиялық материалдар жастардың еркін араласу, салауатты өмір салтын ұстану және қиын жағдайларда өзін қорғау дағдыларын қалыптастыруға көмектеседі. Сондай-ақ, берілген материалдар мамандық таңдауда да жол сілтейтін көмекші құрал бола алады.",
        "What does the project propose to students?",
        "The web-portal provides multimedia materials for students. The provided materials help students to gain skills of communication, skills of supporting their healthy lifestyle and skills of protecting themselves in critical situations. Besides, materials can serve as tutorials for choosing a future profession.",
        true,
        true
        );