CREATE TABLE `new_types` (
	`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`key` varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE news (
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `content` longtext NOT NULL,
    `picture` varchar(255) DEFAULT NULL,
    `video` varchar(255) DEFAULT NULL,
    `new_type_id` int(10) UNSIGNED NOT NULL,
    `group_id` int(10) UNSIGNED NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP NOT NULL DEFAULT NOW(),
    `language` enum('ru', 'kz') DEFAULT 'ru' ,
    PRIMARY KEY (id),
    FOREIGN KEY (group_id) REFERENCES groups(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (new_type_id) REFERENCES new_types(id) ON DELETE CASCADE ON UPDATE CASCADE
);

insert into new_types (`name`, `key`) values ('Литература', 'literature');
insert into new_types (`name`, `key`) values ('Новость', 'new');
insert into new_types (`name`, `key`) values ('Инструкция', 'instruction');


