alter table profiles add column gender enum('m', 'f');
update profiles set gender = 'm' where sex != '2';
update profiles set gender = 'f' where sex = '2';