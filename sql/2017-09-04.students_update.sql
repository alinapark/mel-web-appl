alter table students add column state_at_interruption text;
alter table students add column interrupted_at int(11) DEFAULT NULL;