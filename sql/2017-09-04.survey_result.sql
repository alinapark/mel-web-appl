DROP TABLE IF EXISTS `survey_results`;
CREATE TABLE `survey_results` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,

  `suicide_thoughts_scale` int(11) DEFAULT NULL,

  `depression_scale` int(11) DEFAULT NULL,
  `anxiety_scale` int(11) DEFAULT NULL,
  `stress_scale` int(11) DEFAULT NULL,

  `emotional_symptoms_scale` int(11) DEFAULT NULL,
  `behavioral_problems_scale` int(11) DEFAULT NULL,
  `hyperactivity_scale` int(11) DEFAULT NULL,
  `peer_problems_scale` int(11) DEFAULT NULL,
  `prosocial_behavior_scale` int(11) DEFAULT NULL,
  `total_problems_scale` int(11) DEFAULT NULL,

  `is_suicide_heavy` bit(1) NOT NULL DEFAULT b'0',
  `is_in_risk_group` bit(1) NOT NULL DEFAULT b'0',
  `is_self_damaging` bit(1) NOT NULL DEFAULT false,

  `suicide_method` text,
  `has_got_medical_help` bit(1) NULL DEFAULT NUlL,
  `has_not_talked_to_someone` bit(1) NULL DEFAULT NULL,

  `has_tried_to_commit_suicide` bit(1) NULL DEFAULT NULL,

  `q1` int(11) DEFAULT NULL,
  `q2` int(11) DEFAULT NULL,
  `q3` int(11) DEFAULT NULL,

  `all_questions_json` text,

  `student_id` int(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT NOW(),

  FOREIGN KEY(`student_id`) REFERENCES students(id),
  PRIMARY KEY(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;