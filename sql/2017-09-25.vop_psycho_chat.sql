DROP TABLE IF EXISTS students_and_accounts;
CREATE TABLE students_and_accounts (
    student_id int(10) UNSIGNED,
    account_id int(10) UNSIGNED,
    PRIMARY KEY (student_id, account_id),
    FOREIGN KEY (student_id) REFERENCES students(id),
    FOREIGN KEY (account_id) REFERENCES accounts(id)
);