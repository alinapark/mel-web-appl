DROP TABLE IF EXISTS leading_conversation_messages;
CREATE TABLE leading_conversation_messages (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    student_id int(10) UNSIGNED,
    author_id int(10) UNSIGNED,
    author_profile_id int(10) UNSIGNED,
    parent_message_id int(10) UNSIGNED,
	message text,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id),
    FOREIGN KEY (student_id) REFERENCES students(id),
    FOREIGN KEY (author_profile_id) REFERENCES profiles(id),
    FOREIGN KEY (parent_message_id) REFERENCES leading_conversation_messages(id),
    FOREIGN KEY (author_id) REFERENCES accounts(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;