package junit.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import utils.PdfCreator;

import java.io.File;

public class PdfCreatorTest {

    @Test
    public void testGenerateSimplePdf() {
        final String html = "<h1> Hello World </h1>";
        final String code = "123";
        try {
            final File result = PdfCreator.fromAnswersHtml(code, html);
            assertTrue("", result.length() > 0);
        }
        catch (Exception ex) {
            fail("Exception is not expected");
        }


    }
}
