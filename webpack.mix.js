const { mix } = require('laravel-mix');

mix
    .options({
        processCssUrls: false
    })

    // Frontend
    .sass('resources/assets/sass/app.scss', 'public/css')
    .js('resources/assets/js/app.js', 'public/js')
    .copy('resources/assets/js/home.js', 'public/js')

    // Fonts
    .copy('resources/assets/fonts/*', 'public/css/fonts')

    // Libs
    .copy('resources/assets/js/libs/chosen.jquery.js', 'public/js/libs')
    .copy('resources/assets/js/libs/chat_scripts.js', 'public/js/libs')
    .copy('resources/assets/js/libs/faq_entities.js', 'public/js/libs')
    .copy('resources/assets/js/libs/sticky-rows.js', 'public/js/libs')
    .copy('resources/assets/js/libs/sticky-rows2.js', 'public/js/libs')
    .copy('resources/assets/js/libs/jquery.scroolly.min.js', 'public/js/libs')
    .copy('resources/assets/js/libs/owl.carousel.min.js', 'public/js/libs')

    // Backend
    .sass('resources/assets/admin/sass/app.scss', 'public/css/admin')
    .js('resources/assets/admin/js/app.js', 'public/js/admin')

    // Font-Awesome
    .copy('node_modules/font-awesome/fonts/*', 'public/css/admin/fonts')

    // Scripts
    .js('resources/assets/admin/js/libs/tinymce.js', 'public/js/admin/libs/tinymce.js')
    .js('resources/assets/admin/js/libs/onleave.js', 'public/js/admin/libs/onleave.js')

    .copy('resources/assets/monitoring/js/monitoring.js', 'public/js/monitoring.js')
    .copy('resources/assets/monitoring/css/monitoring.css', 'public/css/monitoring.css')

    // Modules
    .copy('resources/assets/admin/js/modules/account.js', 'public/js/admin/modules/account.js')
    .copy('resources/assets/admin/js/modules/location.js', 'public/js/admin/modules/location.js')
    .copy('resources/assets/admin/js/modules/quiz.js', 'public/js/admin/modules/quiz.js')
    .copy('resources/assets/admin/js/modules/stepable.js', 'public/js/admin/modules/stepable.js')

    // Libs
    .copy('resources/assets/admin/js/libs/jquery-ui.js', 'public/js/admin/libs/jquery-ui.js')
    .copy('resources/assets/admin/js/libs/datepicker.js', 'public/js/admin/libs/datepicker.js')
    .copy('resources/assets/admin/sass/libs/datepicker.css', 'public/css/admin/libs/datepicker.css')

    .version();
