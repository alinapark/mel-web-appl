import React  from 'react';
import {BrowserRouter, Switch, Route } from "react-router-dom";

import "./z_css/App.css";

import Auth from "./Auth";
import Monitoring from "./Monitoring";



const App = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/monitoring' component={Auth}/>
            <Route path='/monitoring' component={Monitoring}/>
        </Switch>
    </BrowserRouter>
)

export default App;
