import React, { Component } from 'react';

import './z_css/Auth.css';

import AuthCardOuter from './auth-card-outer/auth-card-outer';
import AuthCard from './auth-card/auth-card';

import * as fakeAPI from "../fakeAPI";
import LoaderCustom from "./loader-custom/loader-custom";



class Auth extends Component {
    constructor() {
        super();
        this.state = {
            login: "",
            password: "",
            errorMessage: null,
            isLoading: false,
            render1: false,
            render2: false
        }
    }

    componentWillMount() {
        // validation function
        fetch('/monitoring/api/validate', {
            credentials: "same-origin"
        })
            .then((res) => {
                if (res.status !== 200 || !res.ok) {
                    

                    throw new Error("Не валиден!");
                }

                return res.json();
            })
            .then((json) => {
                if (json.type == "republic") {
                    this.props.history.replace('monitoring/republic');
                } else if (json.type == "region") {
                    this.props.history.replace(`monitoring/region/${json.regionId}`);
                } else if (json.type == "province") {
                    this.props.history.replace(`monitoring/province/${json.provinceId}`);
                } else {
                    this.setState({
                        render: true,
                        isLoading: false,
                        errorMessage: "Ошибка при попытке валидации"
                    });
                }
            })
            .catch((e) => {
                console.log(e);

                this.setState({
                    render: true
                });
            })
    }

    handleLoginInput = (e) => {
        this.setState({
            login: e.target.value
        })
    }

    handlePasswordInput = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    handleLoginSubmit = (e) => {
        //turn on loading
        this.setState({
            isLoading: true
        });

        fetch(`/monitoring/api/login?login=${this.state.login}&password=${this.state.password}`, {
            credentials: "same-origin"
        })
            .then((response) => {

                if (response.status !== 200 || !response.ok) {
                    throw new Error("Ошибка при попытке логина");
                }

                return response.json();
            })
            .then((json) => {
                if (json.type == "republic") {
                    this.props.history.replace('monitoring/republic');
                } else if (json.type == "region") {
                    this.props.history.replace(`monitoring/region/${json.regionId}`);
                } else if (json.type == "province") {
                    this.props.history.replace(`monitoring/province/${json.provinceId}`);
                } else {
                    this.setState({
                        isLoading: false,
                        errorMessage: "Ошибка при попытке логина"
                    });
                }
                
            })
            .catch((e) => {
                this.setState({
                    isLoading: false,
                    errorMessage: e.message
                })
            });
    }

    render() {


        if (!this.state.render) {
            return (
                <LoaderCustom />
            )
        }

        return (
          	<div className="container auth">
                <AuthCardOuter>
                    <AuthCard 
                        handleLoginSubmit={this.handleLoginSubmit}
                        handleLoginInput={this.handleLoginInput}
                        handlePasswordInput={this.handlePasswordInput}
                        isLoading={this.state.isLoading}
                        errorMessage={this.state.errorMessage}
                    />
                </AuthCardOuter>
            </div>
        )
    }
}

export default Auth;
