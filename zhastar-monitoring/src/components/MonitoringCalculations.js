import * as fakeAPI from "../fakeAPI";
import { getURLwithoutRoot } from "../helpers";

export const fetchData = (props, setState) => {
	const urlWithoutRoot = getURLwithoutRoot(props.location.pathname);

	fetch("/monitoring/api" + urlWithoutRoot, {
		credentials: "same-origin"
	})
		.then((response) => {
			if (response.status !== 200 || !response.ok) {
				throw new Error("Ошибка запроса!");
			}

			return response.json();
		})
		.then((json) => {
			setState({
				data: json,
				render1: true
			})
		})
		.catch((e) => {
			props.history.replace("/monitoring");
		});
}

export const isBackRender = (props, state) => {
	let republicRender = state.accountType === "republic" && props.location.pathname !== '/monitoring/republic';
	let regionRender = state.accountType === "region" && !props.location.pathname.includes('/monitoring/region');
	let provinceRender = state.accountType === "province" && !props.location.pathname.includes('/monitoring/province');

	return republicRender || regionRender || provinceRender;
}

export const validate = (setState) => {
	fetch('/monitoring/api/validate', {
	    credentials: "same-origin"
	})
	    .then((res) => {
	        if (res.status !== 200 || !res.ok) {
	            
	            throw new Error("Не валиден!");
	        }

	        return res.json();
	    })
	    .then((json) => {
            setState({
                accountType: json.type,
                render2: true
            });
	    })
	    .catch((e) => {
	        console.log(e);

	        setState({
	            render2: true
	        });
	    });
}

export const selectMainPanel = (state) => {
	const { data } = state;

	return {
		name: data.name,
		main1: {
			title: "Группа риска", 
			data: [data.maleCount, data.femaleCount],
			labels: [`Мужской - ${data.maleCount}`, `Женский - ${data.femaleCount}`],
			centerText: data.sum,
			centerSubtext: null,
			colors: ['#00897B','#E64A19']
		},
		main2: {
			title: "Попытка суицида",
			data: [56, 76],
			labels: ['Завершенные - 56', 'Незавершенные - 76'],
			centerText: 132,
			centerSubtext: null,
			colors: ['#F44336','#3F51B5']
		},
		main3: {
			title: "Завершенные случаи",
			data: [76, 35],
			labels: ['Мужской - 76', 'Женский - 35'],
			centerText: 111,
			centerSubtext: null,
			colors: ['#2196F3','#F50057']
		}
	}
}

export const selectSecondaryPanelBlock = (state, handlePanelClick) => {
	const { data } = state;
	const { children } = data;

	//handle children filtering

	let secondaryPanelsArrayTest = children
	.sort((a, b) => {
		const { filterSortDropdown } = state;

		if (filterSortDropdown === "alphabet") {
			if (a.name > b.name) {
				return 1;
			} else if (a.name < b.name) {
				return -1;
			} else {
				return 0;
			}
		} else if (filterSortDropdown === "early-det-desc") {
			return b.sum - a.sum
		} else if (filterSortDropdown === "early-det-asc") {
			return a.sum - b.sum
		}
	});

	let secondaryPanelsArray = children
	.sort((a, b) => {
		const { filterSortDropdown } = state;

		if (filterSortDropdown === "alphabet") {
			if (a.name > b.name) {
				return 1;
			} else if (a.name < b.name) {
				return -1;
			} else {
				return 0;
			}
		} else if (filterSortDropdown === "early-det-desc") {
			return b.sum - a.sum
		} else if (filterSortDropdown === "early-det-asc") {
			return a.sum - b.sum
		}
	}).filter((child) => {
		return child.name.toLowerCase().includes(state.filterNameInput.toLowerCase());
	}).map((child) => {

		return {
			"title": child.name,
			secondary1: {
				title: "Группа риска",
				data: [child.maleCount, child.femaleCount],
				labels: [`Мужской - ${child.maleCount}`, `Женский - ${child.femaleCount}`],
				centerText: child.sum,
				centerSubtext: null,
				colors: ['#00897B','#E64A19']
			},
			secondary2: {
				title: "Попытка суицида",
				data: [56, 57],
				labels: ["Завершенные", "Незавершенные"],
				centerText: 113,
				centerSubtext: null,
				colors: ['#F44336','#3F51B5']
			},
			secondary3: {
				title: "Завершенные случаи",
				data: [58, 96],
				labels: ['Мужской', 'Женский'],
				centerText: 154,
				centerSubtext: null,
				colors: ['#2196F3','#F50057']
			},
			clickable: child.type !== "school",
			//pass url parameters for next fetch
			onClick: handlePanelClick.bind(null, child.type, child.id)
		}
	
	});

	/*
		Shape of data created by code below

		[
			[secondaryPanelProps, secondaryPanelProps, secondaryPanelProps],
			[secondaryPanelProps, secondaryPanelProps, secondaryPanelProps],
			...
		]
	*/
	let rowArray = [];
	let secondaryPanelBlockArray = [];

	for (let i = 0; i < secondaryPanelsArray.length; i++) {
		rowArray.push(secondaryPanelsArray[i]);

		if (rowArray.length === 3) {
			secondaryPanelBlockArray.push(rowArray);
			rowArray = [];
		}
	}

	if (rowArray.length !== 0) {
		secondaryPanelBlockArray.push(rowArray);
	}

	return secondaryPanelBlockArray;
}
