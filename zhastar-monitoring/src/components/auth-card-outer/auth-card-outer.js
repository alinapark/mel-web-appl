import React  from 'react';
import "./auth-card-outer.css";

const AuthCardOuter = ({children}) => (
    <div className="auth-card-outer">
    	{ children }
    </div>
)

export default AuthCardOuter;
