import React  from 'react';
import "./auth-card.css";
import PropTypes from "prop-types";

const AuthCard = ({ handleLoginSubmit, handleLoginInput, handlePasswordInput, isLoading, errorMessage}) => (
    <div className="card auth-card monitoring__auth-card">
        <div className="card-content">
        	<p className="title auth-card__title">
        	    Авторизация
        	</p>
        	<div className="field auth-card__input">
        	  <label className="label">Логин</label>
        	  <div className="control">
        	    <input className={`input ${errorMessage ? "is-danger" : ""}`} onChange={handleLoginInput} type="text"/>
        	  </div>
        	</div>

        	<div className="field auth-card__input">
        	  <label className="label">Пароль</label>
        	  <div className="control">
        	    <input className={`input ${errorMessage ? "is-danger" : ""}`} onChange={handlePasswordInput} type="password" />
        	  </div>
        	</div>
        	<div className="control">
        	  <button className={`button is-primary ${isLoading ? "is-loading" : ""}`} onClick={handleLoginSubmit}>Войти</button>
              {errorMessage && <p className="help is-danger">Неправильный логин или пароль</p>}
        	</div>
        </div>
    </div>
)

AuthCard.propTypes = {
    handleLoginSubmit: PropTypes.func.isRequired,
    handleLoginInput: PropTypes.func.isRequired,
    handlePasswordInput: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string
}

export default AuthCard;
