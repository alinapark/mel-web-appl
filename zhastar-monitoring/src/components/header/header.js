
import React  from 'react';
import PropTypes from "prop-types";

import "./header.css";

const Header = ({ backRender, handleBackButton, handleLogout, handleLogoLink}) => (
    <nav className="navbar header">
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item header__brand" onClick={handleLogoLink}>
             МОНИТОРИНГ
          </a>
        </div>
        <div className="navbar-menu">
          <div className="navbar-end">
          {backRender &&
            <a className="navbar-item header__back" onClick={handleBackButton}>
              <a className="button is-outlined">
                Назад
              </a>
            </a>
          }
            <a className="navbar-item header__logout" onClick={handleLogout}>
              Выход
            </a>
          </div>
        </div>
      </div>
    </nav>
)


Header.propTypes = {
  backRender: PropTypes.bool.isRequired,
  handleBackButton: PropTypes.func.isRequired,
  handleLogout: PropTypes.func.isRequired,
  handleLogoLink: PropTypes.func.isRequired
}

export default Header;
