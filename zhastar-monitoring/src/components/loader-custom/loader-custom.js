import React  from 'react';
import "./loader-custom.css";

const LoaderCustom = () => (
	<div className="monitoring__loader-custom">
		<div className="loader-custom">
			Loading...
		</div>
	</div>
    
)

export default LoaderCustom;
