import React  from 'react';
import PropTypes from "prop-types";

import "./main-panel.css";
import PanelContent from  "../panel-content/panel-content";

const MainPanel = ({ name, main1, main2, main3 }) => (
    <div className="card main-panel monitoring__main-panel">
        <p className="card-header-title main-panel__title tooltip-ignore">
            { name }
        </p>
        <div className="card-content-small">
            <PanelContent
            	graph1Data={main1}
            	graph2Data={main2}
            	graph3Data={main3}
                isMain={true}
            />
        </div>
    </div>
)

MainPanel.propTypes = {
	main1: PropTypes.object.isRequired,
	main2: PropTypes.object.isRequired,
	main3: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired
}

export default MainPanel;
