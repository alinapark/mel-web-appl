import React, { Component } from "react";
import { Doughnut } from 'react-chartjs-2';
import PropTypes from "prop-types";

import "./panel-content__graph.css";

const PanelContentGraph = ({ graphData, panelContentGraphClassNames, isMain, graphBackgroundColors } ) => (
    <div className={`panel-content__graph ${panelContentGraphClassNames !== undefined ? panelContentGraphClassNames : ""}`}>
    	<Doughnut
    		height={100}
    		width={50}
    		data = {{
    		    datasets: [{
    		        data: graphData.data,
        		    backgroundColor: graphData.colors
    		    }],

    		    labels: graphData.labels
    		}}
    		options={{
    			legend: {
    				position: 'bottom',
    				display: isMain,
                    labels: {
                        fontSize: 12,
                        boxWidth: 20
                    }
    			},
    			maintainAspectRatio: false,
    			title: {
    				display: true,
    				text: graphData.title,
                    fontSize: isMain ? 18 : 12
    			},
    			[`${!isMain && 'animation'}`]: isMain,
    			tooltips: {
    				enabled: false
    			}
    		}}
    	/>
    	<div className={`panel-content__graph-inside ${!isMain ? "panel-content__graph-inside_small" : ""}`}>
    	    <p className={`panel-content__graph-number ${!isMain ? "panel-content__graph-number_small" : ""}`}>{graphData.centerText}</p>
    	</div>
    </div>
)

PanelContentGraph.propTypes = {
    panelContentGraphClassNames: PropTypes.arrayOf(PropTypes.string),
    graphData: PropTypes.shape({
    	title: PropTypes.string.isRequired,
    	data: PropTypes.arrayOf(PropTypes.number).isRequired,
    	labels: PropTypes.arrayOf(PropTypes.string).isRequired,
    	centerText: PropTypes.number,
    	centerSubtext: PropTypes.string,
    	colors: PropTypes.arrayOf(PropTypes.string).isRequired
    }).isRequired,
    isMain: PropTypes.bool.isRequired
}


export default PanelContentGraph;