import React from 'react';
import PropTypes from 'prop-types';

import SecondaryPanel from '../../secondary-panel/secondary-panel';

const SecondaryPanelBlockRow = ({ row }) => (
	<div className="columns secondary-panel-block__row">
		{ row.map(panel => (
			<div className="column is-one-third" key={row.indexOf(panel)}>
				<SecondaryPanel {...panel}/>	
			</div>
		))}
	</div>
)

SecondaryPanelBlockRow.propTypes = {
	row: PropTypes.arrayOf(PropTypes.any).isRequired
}

export default SecondaryPanelBlockRow;