const republicData = {
id: 0,
name: "Казахстан",
urgentSituationCount: 3,
highRiskCount: 0,
lowRiskCount: 1,
noRiskCount: 2,
sum: 6,
children: [
	{
	id: 2,
	name: "Акмолинская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 3,
	name: "Актюбинская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 3,
	highRiskCount: 0,
	lowRiskCount: 1,
	noRiskCount: 2,
	sum: 6,
	type: "region"
	},
	{
	id: 4,
	name: "Алматинская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 5,
	name: "Атырауская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 6,
	name: "Восточно-Казахстанская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 7,
	name: "Жамбылская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 8,
	name: "Западно-Казахстанская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 9,
	name: "Карагандинская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 10,
	name: "Костанайская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 11,
	name: "Кызылординская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 12,
	name: "Мангистауская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 13,
	name: "Павлодарская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 14,
	name: "Северо-Казахстанская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 15,
	name: "Южно-Казахстанская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 221,
	name: "Алматы валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	},
	{
	id: 222,
	name: "Астана валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо",
	urgentSituationCount: 0,
	highRiskCount: 0,
	lowRiskCount: 0,
	noRiskCount: 0,
	sum: 0,
	type: "region"
	}
	],
	type: "country"
}

const regionData = {
id: 2,
name: "Акмолинская область",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
children: [
{
id: 36,
name: "Aршалынский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 37,
name: "Аккольский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 38,
name: "Астраханский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 39,
name: "Атбасарский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 40,
name: "Буландынский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 41,
name: "Бурабайский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 42,
name: "Егиндыкольский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 43,
name: "Енбекшильдерский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 44,
name: "Ерейментауский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 45,
name: "Есильский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 46,
name: "Жаксынский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 47,
name: "Жаркаинский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 48,
name: "Зерендинский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 49,
name: "Коргалжынский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 50,
name: "Сандыктауский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 51,
name: "Целиноградский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 52,
name: "Шортандинский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 53,
name: "Кокшетау",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
},
{
id: 54,
name: "Степногорск",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "province"
}
],
type: "region"
}

const provinceData = {
id: 36,
name: "Aршалынский район",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
children: [
{
id: 1650,
name: "Аршалынская СШ № 1",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1657,
name: "Аршалынская СШ № 3",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1658,
name: "Волгодоновская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1661,
name: "Вячеславская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1663,
name: "Ижевская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1666,
name: "Новоалександровская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1676,
name: "СШ им.Кутпанулы",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 1677,
name: "Тургеневская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 2057,
name: "НШ №149",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 3090,
name: "Центральная СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 3091,
name: "АСШ № 2",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 3092,
name: "Михайловская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 3112,
name: "Берсуатская СШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
},
{
id: 3153,
name: "Херсоновская ОШ",
urgentSituationCount: 0,
highRiskCount: 0,
lowRiskCount: 0,
noRiskCount: 0,
sum: 0,
type: "school"
}
],
type: "province"
}

export const fetch = (url) => {
	return new Promise((resolve) => {
		if (url === '/monitoring/api/republic') {
 	 		setTimeout(() => {
 	 			resolve({
					status: 200,
					ok: true,
					json: () => new Promise((resolve) => resolve(republicData))
				})
 	 		}, 2000);
		} else if (url === '/monitoring/api/region/2') {
			setTimeout(() => {
				resolve({
					status: 200,
					ok: true,
					json: () => new Promise((resolve) => resolve(regionData))
				})
			}, 2000)
		} else if (url === '/monitoring/api/province/36') {
			setTimeout(() => {
				resolve({
					status: 200,
					ok: true,
					json: () => new Promise((resolve) => resolve(provinceData))
				})
			}, 2000)
		} else if (url === '/monitoring/api/login?login=tauka&password=123123') {
			setTimeout(() => {
				resolve({
					status: 200,
					ok: true,
					text: () => new Promise((resolve) => resolve(provinceData))
				})
			}, 2000)
		} else if (url === '/monitoring/api/validate') {
			setTimeout(() => {
				resolve({
					status: 200,
					ok: true,
					json: () => new Promise((resolve) => resolve({
						type: "republic"
					}))
				})
			}, 2000)
		} else {
			setTimeout(() => {
				resolve({
					status: 500,
					ok: false
				})
			}, 2000)
		}
	})
}

export const fetchUnauthorized = () => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve({
				data: "login"
			})
		}, 2000);
	})
}

export const login = (url, credentials) => {
	return new Promise((resolve) => {
		if (credentials.login === "tauka" && credentials.password === "123123") {
			setTimeout(() => {
				resolve({
					data: {
						login: "tauka",
						role: "doctor",
						registered: new Date()
					}
				})
			}, 2000)	
		} else {
			setTimeout(() => {
				resolve({
					data: "login"
				})
			}, 2000)
		}
		
	})
}