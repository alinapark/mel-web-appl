import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import "bulma/css/bulma.css";


import './index.css';
import App from "./components/App";
import "./plugins/resizeListener";
import "./plugins/legendTextSizeListener";

ReactDOM.render(
	<App/>, document.getElementById('root'));
registerServiceWorker();
