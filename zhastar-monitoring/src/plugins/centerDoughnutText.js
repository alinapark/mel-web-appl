const baseFontSize = 105;
const baseFontSizeSmall = 32;
const grandParentWidthBase = 379;
const grandParentHeightBase = 650;


export default function() {
	let centerTexts = document.getElementsByClassName("panel-content__graph-number");

	let topOffset = 5 - (4320 / window.innerWidth - 3);
	const topOffsetSmall = 15;

	console.log("topOffset", topOffset);

	for (let i = 0; i < centerTexts.length; i++) {
		let grandParent = centerTexts[i].parentElement.parentElement;
		let parent = centerTexts[i].parentElement;

		
		let grandParentWidth = window.getComputedStyle(grandParent, null).getPropertyValue("width");
		let grandParentHeight = window.getComputedStyle(grandParent, null).getPropertyValue("height");

		let base = parseFloat(grandParentWidth.slice(0, -2)) < parseFloat(grandParentHeight.slice(0, -2)) ? grandParentWidthBase : grandParentHeightBase;
		// console.log("Width", grandParentWidth);
		// console.log("Height", grandParentHeight);
		// console.log("Base", base);
		// console.log("Font calc", baseFontSize * (parseFloat(grandParentWidth.slice(0, -2)) / base)  * (1 - centerTexts[i].innerHTML.length / 10));
		centerTexts[i].style.fontSize = baseFontSize * (parseFloat(grandParentWidth.slice(0, -2)) / base)  * (1 - centerTexts[i].innerHTML.length / 10) + "px";

		let width = window.getComputedStyle(centerTexts[i], null).getPropertyValue("width");
		let height = window.getComputedStyle(centerTexts[i], null).getPropertyValue("height");

		var widthNumber = parseFloat(width.slice(0, -2));
		var heightNumber = parseFloat(height.slice(0, -2));
		var grandParentWidthNumber = parseFloat(grandParentWidth.slice(0, -2));
		var grandParentHeightNumber = parseFloat(grandParentHeight.slice(0, -2));

		let topValue = 0;

		if (centerTexts[i].classList.contains("panel-content__graph-number_small")) {
			topValue = (grandParentHeightNumber / 2) - (heightNumber / 2) + topOffsetSmall;
		} else {
			topValue = (grandParentHeightNumber / 2) - (heightNumber / 2) + topOffset;
		}

		let leftValue = (grandParentWidthNumber / 2) - (widthNumber / 2);
		parent.style.left = leftValue + "px";
		parent.style.top = topValue + "px";
	}
}