import { Chart } from 'react-chartjs-2';

Chart.plugins.register({
	beforeInit: function(chart) {	
		if (window.innerWidth < 435) {
			chart.config.options.legend.labels.fontSize = 6;
		} else if (window.innerWidth < 600) {
			chart.config.options.legend.labels.fontSize = 8;
		} else {
			chart.config.options.legend.labels.fontSize = 12;
		}
	},
	beforeRender: function(chart) {	
		if (window.innerWidth < 435) {
			chart.config.options.legend.labels.fontSize = 6;
		} else if (window.innerWidth < 600) {
			chart.config.options.legend.labels.fontSize = 8;
		} else {
			chart.config.options.legend.labels.fontSize = 12;
		}
	}
});