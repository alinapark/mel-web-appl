var webpack = require('webpack');
var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: path.join(__dirname),
  devtool: false,
  entry: "./index.js",
  module: {
    rules: [
      {
        test: /\.css$/,
        loader: "style-loader!css-loader" 
      },
      {
        test: /\.svg/, 
        loader: 'svg-url-loader'
      },
      { 
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader:"url-loader" 
      },
      { 
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" 
      },
    ]
  },
  output: {
    path: __dirname + "/build/",
    filename: "index.min.js",
    publicPath: '/'
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false
    }),
    new webpack.optimize.UglifyJsPlugin({
      comments: false
    }),
    new webpack.ProvidePlugin({   
            jQuery: 'jquery',
            $: 'jquery',
            "window.jQuery": 'jquery',
            jquery: 'jquery',
            Tether: 'tether',
            "window.Tether": "tether"
    }),
    new CopyWebpackPlugin([{
      from: 'src/files/favicon.ico',
      to: 'files/'
    }])
  ]
};
